<?php echo view('index/0-top'); ?>
<style>
    span.text {
        font-family: 'Courier New', Courier, monospace !important;
    }

    a.dropdown-item {
        padding: 0px 10px !important;
    }

    .line-number {
        background: url(<?php echo  base_url('public/img/line-number.png'); ?>);
        background-attachment: local;
        background-repeat: no-repeat;
        padding-left: 35px !important;
        padding-top: 11px !important;
        font-size: 11px !important;
        border-color: #ccc;
    }

    textarea {
        resize: none;
    }

    .highlight_tertugas {
        color: red !important;
        font-weight: bold !important;
    }

    .highlight_tujuan {
        background: yellow !important;
    }
</style>

<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label"><?php echo $page; ?></h3>
        </div>
        <div class="card-toolbar">
            <!-- <button type="button" class="btn btn-primary tambahST">
                <i class="fas fa-folder-plus"></i>
                Tambah ST
            </button> -->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <!-- <div class="table-responsive"> -->
        <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th style="width: 25%;">ST & Kegiatan</th>
                    <th style="width: 25%;">SPD</th>
                    <th style="width: 35%;">Kuitansi</th>
                    <th style="width: 15%;">Aksi</th>
                </tr>
            </thead>
        </table>
        <!-- </div> -->
        <!--end: Datatable-->
    </div>
</div>


<!-- Modal SPD -->
<div class="modal fade" id="modalSPD" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="FormSPD">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Tanggal SPD
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="hidden" name="id_st" value="">
                                <input type="hidden" name="id_spd" value="0">
                                <input type="text" class="form-control" name="tgl_spd" value="<?php echo ''; ?>" required />

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Tanggal Kuitansi</label>
                                <input type="text" class="form-control" name="tgl_kuitansi" value="<?php echo ''; ?>" required />

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Lokasi
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="lokasi_spd" value="<?php echo $referensi_st['lokasi_spd'];; ?>" />

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleTextarea">Tingkat Biaya
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="tingkat_spd" value="<?php echo $referensi_st['tingkat_spd'];; ?>" />

                            </div>
                        </div>

                        <div class="col-lg-9">
                            <div class="form-group">
                                <label for="exampleTextarea">Anggaran - Instansi
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="anggaran_instansi" value="<?php echo $referensi_st['anggaran_instansi'];; ?>" />

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Anggaran
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="anggaran" value="<?php echo $referensi_st['anggaran'];; ?>" />
                            </div>
                        </div> -->
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Anggaran - Akun
                                    <span class="text-danger">*</span>
                                </label>
                                <select class="form-control font-size-sm" name="id_mak" required>
                                    <option>- pilih mak -</option>
                                    <?php foreach ($mak as $ak) { ?>
                                        <option value="<?php echo $ak->id_mak; ?>"><?php echo $ak->kode_mak . '-' . $ak->detail_mak; ?></option>
                                    <?php }; ?>
                                </select>

                            </div>
                        </div>
                        <!-- <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleTextarea">Anggaran - Akun
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="anggaran_akun" value="<?php echo $referensi_st['anggaran_akun'];; ?>" />

                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Pejabat Pembuat Komitmen
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            Nama:
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="nama_ppk" value="<?php echo $referensi_st['nama_ppk']; ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            NIP:
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="nip_ppk" value="<?php echo $referensi_st['nip_ppk']; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="exampleTextarea">Bendahara
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            Nama:
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="nama_bendahara" value="<?php echo $referensi_st['nama_bendahara']; ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            NIP:
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="nip_bendahara" value="<?php echo $referensi_st['nip_bendahara']; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Transportasi</label>
                                <div class="checkbox-inline">
                                    <label class="checkbox">
                                        <input type="hidden" name="trans_darat" value="0">
                                        <input type="checkbox" name="trans_darat" value="1">
                                        <span></span>Darat</label>
                                    <label class="checkbox">
                                        <input type="hidden" name="trans_laut" value="0" />
                                        <input type="checkbox" name="trans_laut" value="1">
                                        <span></span>Laut</label>
                                    <label class="checkbox">
                                        <input type="hidden" name="trans_udara" value="0" />
                                        <input type="checkbox" name="trans_udara" value="1">
                                        <span></span>Udara</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Kuitansi-->
<div class="modal fade" id="modalKuitansi" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form class="form" id="formKuitansi">
                <div class="modal-header">
                    <span class="text-right col">
                        <button type="button" class="btn btn-success font-weight-bold" id="tambahBiaya" id_tertugas="" id_st="">Tambah Biaya</button>
                    </span>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Nama</label>
                                <input class="form-control" id="nama_tertugas" disabled>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tujuan</label>
                                <input class="form-control" id="tujuan_dinas" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Detail Biaya</label>
                                <table class="table table-sm table-bordered" id="tabelBiaya">
                                    <thead>
                                        <tr>
                                            <th scope="col"><i class="fas fa-sort-numeric-down"></i></th>
                                            <th scope="col">Jenis</th>
                                            <th scope="col">Rincian</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Biaya -->
<div class="modal fade  modal-danger" id="modalBiaya" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="formBiaya">
                <div class="modal-body bg-primary-o-50">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <table class="table table-bordered" id="referensiSBM">
                                    <thead>
                                        <tr>
                                            <td class="text-center">Referensi</td>
                                            <td class="text-center">Nilai</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="offset-lg-2 col-lg-5">

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Jenis
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="hidden" name="id_biaya" value="0" />
                                        <input type="hidden" name="biaya_id_st" />
                                        <input type="hidden" name="biaya_id_tertugas" />
                                        <select class="form-control" name="kode_biaya">
                                            <option value=""></option>
                                            <option value="1" placeholder="Tiket aaaa-bbbb">Transportasi Udara</option>
                                            <option value="2" placeholder="Tiket Kapal aaaa-bbbb">Transportasi Laut</option>
                                            <option value="3" placeholder="Transport aaaa-bbbb">Transportasi Darat</option>
                                            <option value="4" placeholder="Uang Harian (nnnn hari x Rp mmmm)">Uang Harian</option>
                                            <option value="5" placeholder="Uang Hotel (nnnn hari x Rp mmmm)">Uang Hotel</option>
                                            <option value="6">Lain-lain</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Urutan
                                        </label>
                                        <input type="number" class="form-control" name="urutan" min=0 max=99 />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Rincian
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="rincian_biaya" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Nominal
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="number" class="form-control" name="nominal_biaya" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Keterangan
                                        </label>
                                        <input type="text" class="form-control" name="keterangan_biaya" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal Edit Biaya -->
<div class="modal fade  modal-danger" id="modalEditBiaya" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="formEditBiaya">
                <div class="modal-body bg-primary-o-50">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="exampleTextarea">Jenis
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="hidden" name="id_biaya" id="idBiayaEdit" />
                                <input type="hidden" id="idTertugasEdit" />
                                <select class="form-control" name="kode_biaya" disabled>
                                    <option value=""></option>
                                    <option value="1" placeholder="Tiket PP aaaa-bbbb">Transportasi Udara</option>
                                    <option value="2" placeholder="Tiket Kapal aaaa-bbbb">Transportasi Laut</option>
                                    <option value="3" placeholder="Transport aaaa-bbbb">Transportasi Darat</option>
                                    <option value="4" placeholder="Uang Harian (nnnn x mmmm)">Uang Harian</option>
                                    <option value="5" placeholder="Uang Hotel (nnnn x mmmm)">Uang Hotel</option>
                                    <option value="6">Lain-lain</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Urutan
                                </label>
                                <input type="number" class="form-control" name="urutan" min=0 max=99 />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Rincian
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="rincian_biaya" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nominal
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="nominal_biaya" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Keterangan
                                </label>
                                <input type="text" class="form-control" name="keterangan_biaya" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Viewer-->
<div class="modal fade" id="viewerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document" style="height: 95vh; display: flex;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="embedViewer" src="" frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%"></iframe>
            </div>
        </div>
    </div>
</div>
<!--end::Card-->
<?php echo view('index/i-js-global'); ?>


<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script>
    function convertRP(bilangan) {
        var reverse = bilangan.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);

        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }
    var refSPD;
    $.getJSON('<?php echo base_url('api/referensi/spd'); ?>', function(data) {
        refSPD = data;
    });
    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    $('input[name ="tgl_spd"]').datepicker({
        autoclose: true,
    });
    $('input[name ="tgl_kuitansi"]').datepicker({
        autoclose: true,
    });


    $(document).ready(function() {
        var oTable = $('#kt_datatable').DataTable({
            pageLength: 5,
            lengthMenu: [
                [5, 10, 20, -1],
                [5, 10, 20, 'All']
            ],
            'order': [
                [0, 'desc']
            ],
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url(); ?>/api/datatables/spd",
            columns: [{
                    data: 'no_st',
                    className: 'align-top '
                },
                {
                    data: 'data_spd',
                    className: 'align-top'
                },
                {
                    data: 'petugas',
                    className: 'align-top'
                },
                {
                    data: 'id_st',
                    className: 'align-top'
                },
            ],
            columnDefs: [{
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {

                        // console.log(full);
                        var kuitansiOK = true;
                        $.each(full.tertugas_st, function(key, val) {
                            if ($.isEmptyObject(val['biaya'])) {
                                // console.log(key);
                                var kuitansiOK = false;
                            }
                        });

                        return '\
                        ' + (full.data_spd !== null ? '<span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-outline-danger font-size-sm cetakSPDAll"><i class="la la-print"></i> Semua SPD</span>' : '') + '\
                        ' + ((full.data_spd !== null && kuitansiOK) ? '<span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-outline-warning font-size-sm cetakKuitansiAll"><i class="la la-trash"></i> Semua Kuitansi</span>' : '') + '\
						';
                    },
                }, {
                    targets: 0,
                    render: function(data, type, full, meta) {

                        if (data === null) {
                            return '<span class="btn text-left btn-block align-left my-1 py-1 btn-xs btn-square btn-success font-size-xs edit text-secondary">\
                        <strong>' + full.no_st + '<br>' + full.tgl_st + '</strong>\
                        </span>\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon kegiatan">\
                        <i class="la la-edit"></i>\
                        </a>\
                        ';
                        } else {
                            return '<span class="btn text-left btn-block align-left my-1 py-1 btn-xs btn-square btn-success font-size-xs edit text-secondary">\
                        <strong>' + full.no_st + '<br>' + full.tgl_st + '</strong>\
                        </span>\
                        <span class="btn text-left btn-block align-left my-1 py-1 btn-xs btn-square btn-primary font-size-xs kegiatan">\
                            ' + (full.kegiatan_st) + '\
                        </span>';
                        }
                    },
                }, {
                    targets: 1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var string = '';
                        if (full.data_spd === null) {
                            string += '<button type="button" class="btn btn-outline-danger btn-square btn-sm btn-block buatSPD"><i id_tertugas="' + full.id_st + '" class="icon-sm fa fa-edit"></i> Buat SPD</button>';
                        } else {
                            string += '<button  class="close p-2 text-dark" aria-label="Close"><i aria-hidden="true" id_tertugas="' + full.data_spd.id_spd + '" class="flaticon2-cross text-white"></i></button>\
                                <span class="btn btn-block text-left my-1 pl-3 pr-15 py-1 btn-xs btn-square btn-danger font-size-xs editSPD">\
                                <b class="text-dark-25">' + full.data_spd.tgl_spd.split("-").reverse().join("-") + '</b>\
                                <br><b class="text-light">' + full.data_spd.kode_mak + '</b>\
                                <br>';
                            string += (full.data_spd.trans_darat == 1 ? ' <i class="fas fa-car-side">&nbsp;' : '');
                            string += (full.data_spd.trans_laut == 1 ? ' <i class="fas fa-ship">&nbsp;' : '');
                            string += (full.data_spd.trans_udara == 1 ? ' <i class="fas fa-plane">&nbsp;' : '');
                            string += '</i></span></span>';
                        }
                        return '' + string + '';
                    },
                }, {
                    targets: 2,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        if ($.isEmptyObject(full.tertugas_st) === false) {
                            var string = '';
                            $.each(full.tertugas_st, function(key, val) {
                                var outline = '';
                                var biaya = 0;
                                var kelas = '';
                                var kuitansiOK = true;
                                // console.log(val['biaya']);

                                if ($.isEmptyObject(val['biaya'])) {
                                    outline = 'btn-outline-warning'
                                    biaya = '(buat kuitansi)';
                                    kelas = 'buatKuitansi';
                                    kuitansiOK = false;
                                } else {
                                    $.each(val['biaya'], function(keyx, valx) {
                                        biaya += parseInt(valx['nominal_biaya']);
                                        // console.log(valx['nominal_biaya']);
                                    });
                                    outline = 'btn-warning'
                                    biaya = convertRP(biaya);
                                    kelas = 'editKuitansi';
                                };

                                string += '\
                                <div class="row"><div class="col-lg-8"><span class="' + kelas + ' text-secondary btn btn-block text-left my-1 pl-3 pr-3 py-1 btn-xs btn-square ' + outline + ' font-size-xs"  id_tertugas="' + val['id_tertugas'] + '" harian="' + val['harian'] + '"  hari="' + ((Math.floor((Date.parse(val['tgl_akhir']) - Date.parse(val['tgl_awal'])) / 86400000 + 1))) + '" >\
                                <b class="text-dark float-left">' + val['nama'] + '</b>\
                                <b class="text-black float-right">' + (biaya) + '</b>\
                                <br><b class="text-dark-75  float-left">' + val['nama_kabkota'] + '</b>\
                                <br><span class="float-left"><b class="text-black">' + val['tgl_awal'].split("-").reverse().join("-") + '</b> s.d. <b class="text-black">' + val['tgl_akhir'].split("-").reverse().join("-") + '</b> (<b class="text-black">' + ((Math.floor((Date.parse(val['tgl_akhir']) - Date.parse(val['tgl_awal'])) / 86400000 + 1))) + '</b> hari)</span>\
                                \
                                <br>\
                                </span> \
                                </span>\
                                </div><div class="col-lg-4">\
                                ' + (full.data_spd !== null ? '<span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-light-danger font-size-sm cetakSPD" id_tertugas="' + val['id_tertugas'] + '"><i class="la la-print"></i> SPD</span>' : '') + '\
                                ' + ((full.data_spd !== null && kuitansiOK) ? '<span class="btn text-center btn-block float-center my-1 py-1 btn-square btn-light-warning font-size-sm cetakKuitansi" id_tertugas="' + val['id_tertugas'] + '"><i class="la la-print"></i> Kuitansi</span>' : '') + '\
                                </div></div>\
                            ';

                            });
                            return string;
                        } else {
                            return '';
                        }
                    },
                },

            ],
        });


        $('#FormSPD').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/spd/simpanspd'; ?>',
                data: $('#FormSPD').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#modalSPD").modal('hide');
                        oTable.ajax.reload(null,false);
                    }
                }
            });
        });

        $('#kt_datatable tbody').on('click', '.buatSPD', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            $('#modalSPD').modal('show');
            $('#FormSPD').trigger("reset");
            // console.log(rowData.id_st);
            $('input[name ="tgl_spd"]').datepicker('update', '');
            $('input[name ="tgl_kuitansi"]').datepicker('update', '');
            $('input[name ="id_st"]').val(rowData.id_st);
            $.each(refSPD, function(name, val) {
                $('input[name ="' + name + '"]').val(val);
            });
            $('input[name ="id_spd"]').val(0);
            $('select[name ="id_mak"]').val(0);
            $('input[name ="trans_darat"]').prop('checked', true);
        });

        $('#kt_datatable tbody').on('click', '.editSPD', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            $('#modalSPD').modal('show');
            $('input[name ="id_st"]').val(rowData.id_st);
            $('input[name ="id_spd"]').val(rowData.data_spd.id_spd);
            var trans = ['trans_darat', 'trans_laut', 'trans_udara'];
            // console.log(trans);
            $.each(rowData.data_spd, function(name, val) {
                if ($.inArray(name, trans) > -1) {
                    console.log(val);
                    if (val == 1) {
                        $('input[name ="' + name + '"]').prop('checked', true);
                    } else {
                        $('input[name ="' + name + '"]').prop('checked', false);
                    }
                } else {
                    $('input[name ="' + name + '"]').val(val);
                }
            });
            $('select[name ="id_mak"]').val(rowData.data_spd.id_mak);
            $('input[name ="tgl_spd"]').datepicker('update', rowData.data_spd.tgl_spd.split("-").reverse().join("-"));
            $('input[name ="tgl_kuitansi"]').datepicker('update', rowData.data_spd.tgl_kuitansi.split("-").reverse().join("-"));

        });

        // $('#kt_datatable tbody').on('click', '.cetakSPD', function() {
        //     id_tertugas = $(this).attr('id_tertugas')
        //     window.open('<?php echo base_url(); ?>/cetak/print_spd/index/' + id_tertugas + '/A4', 'spd' + id_tertugas);
        // });


        $('#kt_datatable tbody').on('click', '.cetakSPD', function() {
            id_tertugas = $(this).attr('id_tertugas')
            $('#viewerModal').modal('show');
            $('#embedViewer').attr('src', '<?php echo base_url(); ?>/cetak/print_spd/index/' + id_tertugas + '/A4', 'spd' + id_tertugas);
        });


        $('#kt_datatable tbody').on('click', '.cetakKuitansi', function() {
            id_tertugas = $(this).attr('id_tertugas')
            $('#viewerModal').modal('show');
            $('#embedViewer').attr('src', '<?php echo base_url(); ?>/cetak/print_kuitansi/index/' + id_tertugas + '/A4', 'kuitansi' + id_tertugas);
        });

        $('#kt_datatable tbody').on('click', '.cetakSPDAll', function() {
            id_tertugas = $(this).attr('id_tertugas')
            Swal.fire({
                title: "Sedang dalam pengembangan",
                icon: "info",
            })
            // window.open('<?php echo base_url(); ?>/cetak/print_spd/index/' + id_tertugas, 'spd' + id_tertugas);
        });
        $('#kt_datatable tbody').on('click', '.cetakKuitansiAll', function() {
            id_tertugas = $(this).attr('id_tertugas')
            Swal.fire({
                title: "Sedang dalam pengembangan",
                icon: "info",
            })
            // window.open('<?php echo base_url(); ?>/cetak/print_kuitansi/index/' + id_tertugas, 'kuitansi' + id_tertugas);
        });


        $('#kt_datatable tbody').on('click', '.buatKuitansi', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            tulisBiaya($(this).attr('id_tertugas'), rowData.tertugas_st[$(this).attr('id_tertugas')]);
            var hari = $(this).attr('hari');
            var harian = $(this).attr('harian');
            var tertugas = $(this).attr('id_tertugas');
            $('#modalKuitansi').modal('show');
            $('#FormKuitansi').trigger("reset");
            setTimeout(function() {
                $('#modalBiaya').modal('show');
                // $('input[name ="biaya_id_st"]').val(rowData.id_st);
                // $('input[name ="biaya_id_tertugas"]').val(tertugas);
                // $('select[name ="kode_biaya"]').val(4);
                // $('select[name ="kode_biaya"]').trigger('change');
            }, 700);
        });


        $('#kt_datatable tbody').on('click', '.editKuitansi', function() {
            var rowData = oTable.row($(this).closest("tr.even,tr.odd")).data();
            tulisBiaya($(this).attr('id_tertugas'), rowData.tertugas_st[$(this).attr('id_tertugas')]);
            $('#modalKuitansi').modal('show');
            $('#FormKuitansi').trigger("reset");
        });

        $('#tambahBiaya').click(function() {
            $('#modalBiaya').modal('show');
            $('input[name ="biaya_id_st"]').val($(this).attr('id_st'));
            $('input[name ="biaya_id_tertugas"]').val($(this).attr('id_tertugas'));
            $('select[name ="kode_biaya"]').val(0);
            $('input[name ="rincian_biaya"]').val('');
            $('input[name ="nominal_biaya"]').val('');
            $('input[name ="urutan"]').val('');
        })

        $('#formBiaya').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/spd/simpanBiaya'; ?>',
                data: $('#formBiaya').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#modalBiaya").modal('hide');
                        oTable.ajax.reload(null,false);
                        // console.log($('input[name ="biaya_id_tertugas"]').val());
                        setTimeout(function() {
                            $('.editKuitansi[id_tertugas="' + $('input[name ="biaya_id_tertugas"]').val() + '"]').trigger('click');
                        }, 1000);
                    }
                }
            });
        });


        $('select[name ="kode_biaya"]').on('change', function(e) {
            placeholder = $("option:selected", this).attr('placeholder');
            urutan = 90;
            nominal = '';
            val = $("option:selected", this).val();
            if (val == 1 || val == 2 || val == 3) { //udara laut darat
                if ($('#balik').is(':checked')) {
                    placeholder = placeholder.replace('bbbb', $('#asalDinas').val()).replace('aaaa', $('#tujuanDinas').val());
                } else {
                    placeholder = placeholder.replace('aaaa', $('#asalDinas').val()).replace('bbbb', $('#tujuanDinas').val());
                }
                if (val == 1)
                    urutan = 10;
                else if (val == 2)
                    urutan = 20;
                else
                    urutan = 30;
                $('input[name ="rincian_biaya"]').val(placeholder);
                $('input[name ="urutan"]').val(urutan);
                $('input[name ="nominal_biaya"]').val(nominal);
            } else if (val == 4) { //uang harian
                placeholder = placeholder.replace('mmmm', convertRP($('#uangHarianDinas').val())).replace('nnnn', $('#hariDinas').val());
                nominal = parseInt($('#hariDinas').val()) * parseInt($('#uangHarianDinas').val());
                urutan = 40;
                $('input[name ="rincian_biaya"]').val(placeholder);
                $('input[name ="urutan"]').val(urutan);
                $('input[name ="nominal_biaya"]').val(nominal);
            } else if (val == 5) { //uang hotel
                if ($('#persentase').is(':checked')) {
                    nominal = (parseInt($('#hariDinas').val()) - 1) * parseInt($('#uangHotel').val()) * 3 / 10;
                    placeholder = placeholder.replace('mmmm', convertRP($('#uangHotel').val()) + ' x 30%').replace('nnnn', (parseInt($('#hariDinas').val()) - 1));
                } else {
                    nominal = (parseInt($('#hariDinas').val()) - 1) * parseInt($('#uangHotel').val());
                    placeholder = placeholder.replace('mmmm', convertRP($('#uangHotel').val())).replace('nnnn', parseInt($('#hariDinas').val()) - 1);
                }
                urutan = 50;
                $('input[name ="rincian_biaya"]').val(placeholder);
                $('input[name ="urutan"]').val(urutan);
                $('input[name ="nominal_biaya"]').val(nominal);
            } else if (val == 6) {
                placeholder = 'Lain-lain';
                urutan = 60;
                $('input[name ="rincian_biaya"]').val(placeholder);
                $('input[name ="urutan"]').val(urutan);
                $('input[name ="nominal_biaya"]').val(nominal);
            }
        });

        $('#tabelBiaya tbody').on('click', 'tr td .editBiaya', function() {
            $('#modalEditBiaya').modal('show');
            var tr = $(this).closest('tr');
            console.log(tr.find('td')[0].innerHTML);
            console.log(tr.find('td')[0].innerHTML);
            $('input[name="urutan"]').val(tr.find('td')[0].innerHTML);
            $('select[name="kode_biaya"]').val(tr.find('td')[1].innerHTML);
            $('input[name="rincian_biaya"]').val(tr.find('td')[2].innerHTML);
            $('input[name="nominal_biaya"]').val(tr.find('td')[3].innerHTML.replace(/\./g, ''));
            $('input[name="keterangan_biaya"]').val(tr.find('td')[4].innerHTML);
            $('#idBiayaEdit').val($(this).attr('id_biaya'));
            $('#idTertugasEdit').val($(this).attr('id_tertugas'));
        });


        $('#formEditBiaya').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/perjadin/spd/simpanBiaya'; ?>',
                data: $('#formEditBiaya').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#modalEditBiaya").modal('hide');
                        oTable.ajax.reload(null,false);
                        setTimeout(function() {
                            $('.editKuitansi[id_tertugas="' + $('#idTertugasEdit').val() + '"]').trigger('click');
                        }, 1000);
                    }
                }
            });
        });

        $('#tabelBiaya tbody').on('click', 'tr td .hapusBiaya', function() {
            var tr = $(this).closest('tr');
            var id = $(this).attr('id_biaya');

            Swal.fire({
                title: "Akan menghapus biaya ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Baik",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/perjadin/spd/hapusBiaya/'; ?>' + id,
                        data: {
                            id: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                tr.remove();
                                oTable.ajax.reload(null,false);
                            }
                        }
                    });
                }
            });
        });


        function tulisBiaya(id_tertugas, data) {
            $('#tabelBiaya tbody').html('');
            $.each(data.biaya, function(key, val) {
                $('#tabelBiaya tbody').append('<tr>\
                <td>' + val['urutan'] + '</td>\
                <td>' + val['kode_biaya'] + '</td>\
                <td>' + val['rincian_biaya'] + '</td>\
                <td class="text-right">' + convertRP(val['nominal_biaya']) + '</td>\
                <td>' + val['keterangan_biaya'] + '</td>\
                <td class="text-center">\
                <span class="btn btn-sm btn-danger btn-square hapusBiaya" id_biaya="' + val['id_biaya'] + '">hapus</span>\
                <span class="btn btn-sm btn-success btn-square editBiaya" id_biaya="' + val['id_biaya'] + '" id_tertugas="' + id_tertugas + '">ubah</span>\
                </td>\
                </tr>');
            });
            $('#namaTertugas').html((data['nama']));

            $('#referensiSBM tbody').html('').append('\
            <tr><td>Uang Harian</td><td><input type="number" id="uangHarianDinas" class="form-control text-right" value="' + (data['harian']) + '"></td></tr>\
            <tr><td>Uang Hotel<br/><label class="text-danger">30%&nbsp;<input type="checkbox" id="persentase"></label></td>\
            <td><input type="number" id="uangHotel" class="form-control text-right" value="' + (data['hotel4']) + '"><br/>\
            <div class="btn-group btn-block" role="group" aria-label="Basic example">\
            <button type="button" class="btn btn-outline-primary htl" value="' + (data['hotel1']) + '">Es 1</button>\
            <button type="button" class="btn btn-outline-primary htl" value="' + (data['hotel2']) + '">Es 2</button>\
            <button type="button" class="btn btn-outline-primary htl" value="' + (data['hotel3']) + '">Es 3</button>\
            <button type="button" class="btn btn-outline-primary htl" value="' + (data['hotel4']) + '">Es 4</button>\
            </div>\
            </td></tr>\
            <tr><td>Asal<br/><label class="text-danger">balik&nbsp;<input type="checkbox" id="balik"><span></span> </label></td><td><input id="asalDinas" class="form-control text-right" value="' + (data['kota_asal']) + '"></td></tr>\
            <tr><td>Tujuan</td><td><input id="tujuanDinas" class="form-control text-right" value="' + (data['nama_kabkota']) + '"></td></tr>\
            <tr><td>Jumlah Hari</td><td><input type="number" id="hariDinas" class="form-control text-right" value="' + ((Math.floor((Date.parse(data['tgl_akhir']) - Date.parse(data['tgl_awal'])) / 86400000 + 1))) + '"></td></tr>\
            ');

            $('#tambahBiaya').attr('id_tertugas', data['id_tertugas']).attr('id_st', data['id_st']);
            $('#tujuan_dinas').val(data['nama_kabkota']);
            $('#nama_tertugas').val(data['nama']);

            $('#uangHarianDinas ,#uangHotel, #hariDinas').on('keyup change', function(e) {
                $('select[name ="kode_biaya"]').trigger('change');
            });

            $("#persentase").on('change', function() {
                $('select[name ="kode_biaya"]').trigger('change');
            });

            $("#balik").on('change', function() {
                $('select[name ="kode_biaya"]').trigger('change');
            });

            $('.htl').on('click', function() {
                $('#uangHotel').val($(this).attr('value'));
                $('select[name ="kode_biaya"]').change();
            })
        }

        $("#viewerModal").on('hidden.bs.modal', function() {
            $('#embedViewer').attr('src', '');
        });
    });
</script>
<!--end::Page Scripts-->
<?php echo view('index/0-bottom'); ?>