<!--begin::Card-->
<?php helper('main'); ?>
<h3 class="card-label pb-5"><?php echo $page; ?></h3>
<div class="row">
    <div class="col-lg-4 col-md-12 pb-12">
        <div class="row">
            <div class="col-lg-12 col-md-6">
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="d-flex justify-content-between flex-column h-100">
                            <div class="h-100">
                                <div class="pt-1">
                                    <div class="d-flex align-items-center pb-9">
                                        <div class="symbol symbol-60 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <i class="fas fa-users icon-2x text-primary"></i>
                                            </span>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Pegawai</a>
                                        </div>
                                        <span class="font-weight-bolder label label-xl label-light-primary label-inline px-3 py-5 min-w-45px"><?php echo $count_pegawai; ?></span>
                                    </div>
                                    <div class="d-flex align-items-center pb-9">
                                        <div class="symbol symbol-60 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <i class="fas fa-file-contract icon-2x text-danger"></i>
                                            </span>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a class="text-dark-75 text-hover-danger mb-1 font-size-lg font-weight-bolder">Surat Tugas</a>
                                        </div>
                                        <span class="font-weight-bolder label label-xl label-light-danger label-inline px-3 py-5 min-w-45px"><?php echo $count_st; ?></span>
                                    </div>
                                    <div class="d-flex align-items-center pb-9">
                                        <div class="symbol symbol-60 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <i class="fas fa-file-invoice icon-2x text-success"></i>
                                            </span>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a class="text-dark-75 text-hover-success mb-1 font-size-lg font-weight-bolder">SPD</a>
                                        </div>
                                        <span class="font-weight-bolder label label-xl label-light-success label-inline px-3 py-5 min-w-45px"><?php echo $count_spd; ?></span>
                                    </div>
                                    <div class="d-flex align-items-center pb-9">
                                        <div class="symbol symbol-60 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <i class="fa fa-suitcase icon-2x text-warning"></i>
                                            </span>
                                        </div>
                                        <div class="d-flex flex-column flex-grow-1">
                                            <a class="text-dark-75 text-hover-warning mb-1 font-size-lg font-weight-bolder">Trip</a>
                                        </div>
                                        <span class="font-weight-bolder label label-xl label-light-warning label-inline px-3 py-5 min-w-45px"><?php echo $count_trip; ?></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-6">
                <!--begin::Card-->
                <div class="card card-custom card-stretch">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">Log <small>aktifitas pengguna</small></h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-scroll">
                            <div class="timeline timeline-5">
                                <div class="timeline-items">
                                    <?php foreach ($data_log as $log) {; ?>
                                        <div class="timeline-item">
                                            <div class="timeline-media bg-light-warning"><i class="far fa-dot-circle"></i></div>
                                            <div class="timeline-desc timeline-desc-light-primary">
                                                <span class="font-weight-bolder text-danger"><?php echo ($log->jam); ?></span>
                                                <span class="font-weight-bolder text-primary"><?php echo convertDate($log->tanggal); ?></span>
                                                <p class="font-weight-normal text-dark-50 pb-2">
                                                    <?php echo ($log->username); ?> <?php echo ($log->log_jenis); ?> <?php echo ($log->log_detail == null ? '' : '*'); ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">
                        <h3 class="card-label">Perjalanan Dinas <small>terakhir</small></h3>
                    </h3>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr class="table-secondary">
                            <th class="text-center" width="90px">Tanggal</th>
                            <th class="text-center" colspan="2">Kegiatan</th>
                            <th class="text-center" width="90px">SPD</th>
                            <th class="text-center" width="90px">Kuitansi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data_dinas_terakhir as $dns) {
                            $spd = $dns->spd_null > 0 ? 'times text-danger' : 'check text-success';
                            $kuitansi = $dns->kuitansi_null > 0 ? 'times text-danger' : 'check text-success';
                            $status = ($dns->spd_null) ? 'danger' : (($dns->kuitansi_null) ? 'warning' : 'success');
                            echo "<tr class='table-$status'><th>" . reverseDate($dns->tgl_awal)
                                . "<td class='table-borderless'><i class='fa fa-users text-primary' data-toggle='popover' data-html='true'  data-content='$dns->tertugas'></i></td>"
                                . "</th><td><b>$dns->nama_kabkota</b> : $dns->kegiatan_st</td>"
                                . "<td class='text-center'><i class='fas fa-$spd'></i></td>"
                                . "<td class='text-center'><i class='fas fa-$kuitansi'></i></td</tr>";
                        }; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php echo view('index/i-js-global'); ?>

<script>

</script>