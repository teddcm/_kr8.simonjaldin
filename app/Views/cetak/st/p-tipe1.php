<?php

require_once('public/TCPDF-main/tcpdf.php');
class MYPDF extends TCPDF
{
    public function Footer()
    {
        $this->SetFont('', '', 6);

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'fgcolor' => array(0, 0, 0),
        );
        $this->SetY(-4);
        $this->SetX(1);
        $this->write1DBarcode($this->urutan_st, 'C93', '', '', 200, 3, 0.6, $style, 'N');
        if ($this->is_ds) {
            $this->SetY(-15);
            $Y = $this->getY();
            $this->line(32, $Y, 180, $Y);
            $this->SetFont('times', '', 9);
            $this->Cell(17);
            $this->Cell(5, 0, '-', 0, 0, 'L');
            $this->Cell(0, 0, 'UU ITE No 11 Tahun 2008 Pasal 5 Ayat 1', 0, 1, 'L');
            $this->Cell(17);
            $this->Cell(5, 0, '', 0, 0, 'L');
            $this->Cell(0, 0, '“Informasi Elektronik dan/ atau Dokumen Elektronik dan/ atau hasil cetaknya merupakan alat bukti yang sah.”', 0, 1, 'L');
            $this->Cell(17);
            $this->Cell(5, 0, '-', 0, 0, 'L');
            $this->Cell(0, 0, 'Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan BSre', 0, 1, 'L');
            $this->Image('public/img/logo-bsre.png', 180, $Y + 3, 20, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);
        }
    }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', $size, true, 'UTF-8', false);
$pdf->urutan_st = $st['urutan_st'];
$pdf->is_ds = $is_ds;

// set document information
$pdf->SetAuthor('Teddy Cahyo Munanto');
$pdf->SetTitle('ST ' . str_pad($st['urutan_st'], 3, "0", STR_PAD_LEFT));
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetPrintHeader(false);


// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$kepada = '';
$newline = '';
$petugas = '';
foreach ($tertugas as $t) {
    $kepada .= $newline . '<b>' . ($t['jenisKelamin'] == 'Pria' ? 'Sdr. ' : 'Sdri. ') .
        tulisGelar($t['nama'], $t['gelarDepan'], $t['gelarBelakang']) . '</b> NIP.' . $t['nipBaru'] .
        ' Pangkat ' . $t['pangkatAkhir'] .
        ' Golongan Ruang ' . $t['golRuangAkhir'] .
        ' Jabatan ' . $t['jabatanNama'] .
        //' Pada ' . $t['satuanKerjaKerjaNama'] .
        '';
    $newline = PHP_EOL;
    $petugas .= ' - ' . $t['nama'];
}

$st['dasar_st'] = str_replace('%%nama_dipa%%', $st['nama_dipa'], $st['dasar_st']);
$st['dasar_st'] = str_replace('%%no_dipa%%', $st['no_dipa'], $st['dasar_st']);
$st['dasar_st'] = str_replace('%%tgl_dipa%%', convertDate($st['tgl_dipa']), $st['dasar_st']);

$st['peruntukan_st'] = str_replace('%%nama_dipa%%', $st['nama_dipa'], $st['peruntukan_st']);
$st['peruntukan_st'] = str_replace('%%no_dipa%%', $st['no_dipa'], $st['peruntukan_st']);
$st['peruntukan_st'] = str_replace('%%tgl_dipa%%', convertDate($st['tgl_dipa']), $st['peruntukan_st']);

$data_tujuan = $tujuan[0];
$st['peruntukan_st'] = str_replace('%%kegiatan_st%%', $st['kegiatan_st'], $st['peruntukan_st']);
$st['peruntukan_st'] = str_replace('%%periode_st%%', convertDate(array($data_tujuan['tgl_awal'], $data_tujuan['tgl_akhir'])), $st['peruntukan_st']);
$st['peruntukan_st'] = str_replace('%%lokasi_st%%', trim(preg_replace('/\s\s+/', ' ', $data_tujuan['detail_tujuan'] . ', ' . $data_tujuan['nama_kabkota'] . ', ' . $data_tujuan['nama_prov'])), $st['peruntukan_st']);

$ukuranfontdefault = 11;

//============================================================+
$pdf->AddPage();

//LOGO HEADER
$image_width = 18;
$pdf->Image('public/img/logo_garuda.png', 210 / 2 - ($image_width / 2), 7, $image_width, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);


//HEADER
$pdf->ln(22);
$pdf->SetFont('times', 'B', $ukuranfontdefault + 2);
$text = explode(PHP_EOL, $st['header_st']);
$n = 1;
foreach ($text as $p) {
    $pdf->MultiCell(181, 0, $p, 0, 'C', 0, 1, '', '', true, '', true);
}
$Y = $pdf->GetY();
$pdf->Line(5,  $Y + 3, 210 - 5,  $Y + 3);




$pdf->SetFont('helvetica', 'B', $ukuranfontdefault);
$pdf->ln(5);
$Y = $pdf->getY();
$pdf->Write(0, 'S U R A T   T U G A S', '', 0, 'C', true, 0, false, false, 0);

$pdf->line(85, $Y + 5, 125, $Y + 5);
$pdf->SetFont('', 'B', $ukuranfontdefault);
$pdf->Write(0, 'NOMOR: ' . $st['no_st'], '', 0, 'C', true, 0, false, false, 0);


$pdf->Ln(2);
$pdf->SetFont('', '', $ukuranfontdefault);
$Y = $pdf->GetY();
$X = $pdf->GetX();
$pdf->Cell(17, 5, 'DASAR', 0, 0, 'L');
$pdf->Cell(2, 5, ': ', 0, 0, 'L');
$text = explode(PHP_EOL, $st['dasar_st']);
$pdf->setXY($X, $Y);
$n = 1;
foreach ($text as $p) {
    $pdf->Cell(22);
    $pdf->Cell(5, 5, $n++ . '.', 0, 0, 'L');
    $pdf->MultiCell(150, 5, $p, 0, 'J', 0, 1, '', '', true, '', true);
    $pdf->ln(1);
}


$pdf->Ln(3);
$pdf->SetFont('', 'B', $ukuranfontdefault + 1);
$pdf->Write(0, 'M E N U G A S K A N', '', 0, 'C', true, 0, false, false, 0);

// $pdf->Cell(210, 5, 'M E N U G A S K A N', 0, 1, 'C');



$pdf->Ln(2);
$pdf->SetFont('', '', $ukuranfontdefault);
$Y = $pdf->GetY();
$X = $pdf->GetX();
$pdf->Cell(17, 5, 'KEPADA', 0, 0, 'L');
$pdf->Cell(2, 5, ': ', 0, 0, 'L');
$text = explode(PHP_EOL, $kepada);
$pdf->setXY($X, $Y);
$n = 1;
foreach ($text as $p) {
    $pdf->Cell(22);
    $pdf->Cell(5, 5, $n++ . '.', 0, 0, 'L');
    $pdf->MultiCell(150, 5, $p, 0, 'J', 0, 1, '', '', true, '', true);
    $pdf->ln(1);
}


$pdf->Ln(2);
$Y = $pdf->GetY();
$X = $pdf->GetX();
$pdf->Cell(17, 5, 'UNTUK', 0, 0, 'L');
$pdf->Cell(2, 5, ': ', 0, 0, 'L');
$text = explode(PHP_EOL, $st['peruntukan_st']);
$pdf->setXY($X, $Y);
$n = 1;
foreach ($text as $p) {
    $pdf->Cell(22);
    $pdf->Cell(5, 5, $n++ . '.', 0, 0, 'L');
    $pdf->MultiCell(150, 5, $p, 0, 'J', 0, 1, '', '', true, '', true);
    $pdf->ln(1);
}


$kota_st = 'Banjarbaru';
$tgl_st = convertDate($st['tgl_st']);
$jabatan_spesimen = '';

$nama_spesimen = $st['nama_spesimen'];
$nip_spesimen = $st['nip_spesimen'];
$align_spesimen = 'center';
if ($is_ds) {
    $nama_spesimen = '$';
    $nip_spesimen = '';
    $align_spesimen = 'left';
}
$n = 1;
foreach (explode(PHP_EOL, $st['jabatan_spesimen']) as $p) {
    $jabatan_spesimen .= $p . '<br/>';
}

$tbl_spes = <<<EOD
<div style="page-break-inside:avoid;">
<table cellspacing="0" cellpadding="6" border="0">
<tr>
    <td style="width:60%;"></td>
    <td style="width:15%;" align="left">Ditetapkan di<br/>Pada tanggal</td>
    <td style="width:3%;" align="center">:<br/>:</td>
    <td style="width:23%;" align="left">$kota_st<br/>$tgl_st</td>
</tr>
<tr>
    <td style="width:60%;"></td>
    <td align="left" colspan="3" align="center">$jabatan_spesimen</td>
</tr>
<tr>
    <td align="left" colspan="4" height="5"></td>
</tr>
<tr>
    <td style="width:60%;"></td>
    <td align="left" colspan="3" align="$align_spesimen"><b>$nama_spesimen</b><br/>$nip_spesimen</td>
</tr>
</table>
</div>
EOD;

$pdf->writeHTML($tbl_spes,  0, 0, 0, 0);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('ST_' . date('Y', strtotime($st['tgl_st'])) . ' ' . str_pad($st['urutan_st'], 3, "0", STR_PAD_LEFT) . ' ' . $petugas . '.pdf', 'I');
exit();
//============================================================+
// END OF FILE
//============================================================+