<?php

require_once('public/TCPDF-main/tcpdf.php');
// create new PDF document
$pdf = new TCPDF('P', 'mm', $size, true, 'UTF-8', false);

// set document information
$pdf->SetAuthor('Teddy Cahyo Munanto');
$pdf->SetTitle('Surat Perintah Dinas dan Kuitansi');


// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);



// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 10);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

$header = 'MENTERI KEUANGAN
REPUBLIK INDONESIA';
$nomorspd = str_replace('ST', 'SPD', $tertugas['no_st']);

$namapegawai = tulisGelar($tertugas['nama'], $tertugas['gelarDepan'], $tertugas['gelarBelakang']);
$nippegawai = $tertugas['nipBaru'];
$pangkatpegawai = $tertugas['pangkatAkhir'];
$golonganpegawai = $tertugas['golRuangAkhir'];
$jabatanpegawai = $tertugas['jabatannama'];
$tingkatperjalanan = 'C';

$namakegiatan = trim(preg_replace('/\s\s+/', ' ', $tertugas['kegiatan_st']));
$lokasikegiatan = trim(preg_replace('/\s\s+/', ' ', $tertugas['detail_tujuan'] . ', ' . $tertugas['nama_kabkota'] . ', ' . $tertugas['nama_prov']));

$angkutan = 'Darat dan Pesawat';
$tempatberangkat = 'Kota banjarbaru';
$tempattujuan = $tertugas['nama_kabkota'];


$jumlahhari = floor(1 + (strtotime($tertugas['tgl_akhir']) - strtotime($tertugas['tgl_awal'])) / 86400);
$awalkegiatan = convertDate($tertugas['tgl_awal']);
$akhirkegiatan = convertDate($tertugas['tgl_akhir']);





$nomorsurat = '152/SPD/K/KR.VIII/XI/2020';
$nomorst = '152/ST/K/KR.VIII/XI/2020';
$lokasisurat = 'Banjarbaru';
$tanggalsurat = '30 November 2020';
$tanggalst = '16 November 2020';
$jabatanspesimen = 'Pejabat Pembuat Komitmen';
$namaspesimen = 'Deny Setyawan, SE,MM';
$nipspesimen = '198003272009121001';

$waktukegiatan = '05 s.d. 07 Oktober 2020';
$lokasikegiatan = 'Kantor regional VIII BKN, Jalan Bhayangkara No.1 Kota Banjarbaru, Provinsi Kalimantan Selatan';

$ukuranfontdefault = 10;


//============================================================+
$pdf->AddPage();

//LOGO HEADER
$Y = $pdf->GetY();
$image_width = 18;
$pdf->Image('public/img/logo_garuda.png', 210 / 2 - ($image_width / 2), 7, $image_width, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);

//HEADER
$pdf->ln(22);
$pdf->SetFont('times', 'B', $ukuranfontdefault);
$text = explode(PHP_EOL, $header);
$n = 1;
foreach ($text as $p) {
    $pdf->MultiCell(181, 0, $p, 0, 'C', 0, 1, '', '', true, '', true);
}

$pdf->SetY($Y);
$pdf->SetFont('freesans', '', 6);
$pdf->Cell(120);
$pdf->Cell(0, 2, 'LAMPIRAN II', 0, 2, 'L');
$pdf->Cell(0, 2, 'PERATURAN MENTERI KEUANGAN REPUBLIK INDONESIA', 0, 2, 'L');
$pdf->Cell(0, 2, 'NOMOR : 113/PMK.05/2012', 0, 2, 'L');
$pdf->Cell(0, 2, 'TENTANG', 0, 2, 'L');
$pdf->Cell(0, 2, 'PERJALANAN DINAS JABATAN DALAM NEGERI PEJABAT', 0, 2, 'L');
$pdf->Cell(0, 2, 'NEGARA, PEGAWAI NEGERI, DAN PEGAWAI TIDAK TETAP', 0, 2, 'L');

$Y = $pdf->GetY();
$pdf->SetY($Y);
$pdf->ln(22);

$Y = $pdf->GetY();
$pdf->SetFont('helvetica', '', 9);
$pdf->Cell(0, 4, 'Kementerian Negara / Lembaga :', 0, 1, 'L');
$pdf->SetFont('helvetica', 'B', 9);
$pdf->Cell(0, 4, 'Kantor Regional VIII', 0, 1, 'L');
$pdf->Cell(0, 4, 'Badan Kepegawaian Negara', 0, 1, 'L');

$pdf->SetY($Y);
$pdf->Cell(130);
$pdf->Cell(0, 4, 'Nomor : ' . $nomorspd, 0, 1, 'L');


$pdf->ln(10);
$pdf->SetFont('helvetica', 'B', $ukuranfontdefault);
$pdf->Write(0, 'SURAT PERJALANAN DINAS (SPD)', '', 0, 'C', true, 0, false, false, 0);
$pdf->ln(5);
$pdf->SetFont('', '', 9);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="6" border="1">
<tr>
    <td style="width:5%;text-align:center;">1</td>
    <td style="width:33%;" align="left">Pejabat Pembuat Komitmen</td>
    <td style="width:38%;" align="left">Deny Setyawan, SE,MM</td>
    <td style="width:26%;">NIP. 198003272009121001 </td>
</tr>
<tr>
    <td style="text-align:center;">2</td>
    <td style="" align="left">Nama / NIP Pegawai yang melaksanakan perjalanan dinas</td>
    <td style="" align="left">$namapegawai</td>
    <td>NIP. $nippegawai </td>
</tr>
<tr>
    <td style="text-align:center;">3</td>
    <td style="" align="left">a. Pangkat dan Golongan<br/>b. Jabatan / Instansi<br/>c. Tingkat Biaya Perjalanan Dinas</td>
    <td colspan="2">a. $pangkatpegawai / $golonganpegawai<br/>b. $jabatanpegawai<br/>c. $tingkatperjalanan</td>
</tr>
<tr>
    <td style="text-align:center;">4</td>
    <td style="" align="left">Maksud Perjalanan Dinas</td>
    <td colspan="2">$namakegiatan</td>
</tr>
<tr>
    <td style="text-align:center;">5</td>
    <td style="" align="left">Alat angkutan yang dipergunakan</td>
    <td colspan="2">$angkutan Pesawat dan Angkutan Darat</td>
</tr>
<tr>
    <td style="text-align:center;">6</td>
    <td style="" align="left">a. Tempat berangkat<br/>b. Tempat tujuan</td>
    <td colspan="2">a. $tempatberangkat<br/>b. $tempattujuan</td>
</tr>
<tr>
    <td style="text-align:center;">7</td>
    <td style="" align="left">a. Lamanya perjalanan dinas<br/>b. Tanggal berangkat<br/>c. Tanggal harus kembali</td>
    <td colspan="2">a. $jumlahhari hari<br/>b. $awalkegiatan<br/>c. $akhirkegiatan</td>
</tr>
<tr>
    <td colspan align="center" rowspan="2">8</td>
    <td style="" align="left">Pengikut : Nama</td>
    <td style="text-align:center;"></td>
    <td style="text-align:center;">Keterangan</td>
</tr>
<tr>
    <td style="" align="left">1.<br>2.<br>3.</td>
    <td style="text-align:center;"></td>
    <td style="text-align:center;"></td>
</tr>
<tr>
    <td style="text-align:center;">9</td>
    <td style="" align="left">Pembebanan Anggaran<br/>a. Instansi<br/>b. Akun</td>
    <td colspan="2">01.01.06.3658.001.051.Q.524111<br/>a. KANTOR REGIONAL VIII BKN BANJARMASIN<br/>b. 524111</td>
</tr>
<tr>
    <td colspan align="center">10</td>
    <td colspan align="left" colspan="3">Keterangan lain-lain</td>
</tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Ln(5);
$pdf->Cell(110);
$pdf->Cell(25, 5, 'Ditetapkan di ', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(25, 5, $lokasisurat, 0, 0, 'L');

$pdf->Ln(5);
$pdf->Cell(110);
$X = $pdf->getX();
$pdf->Cell(25, 5, 'Pada tanggal', 0, 0, 'L');
$pdf->Cell(5, 5, ':', 0, 0, 'L');
$pdf->Cell(25, 5, $tanggalsurat, 0, 0, 'L');
$Y = $pdf->getY();
$pdf->line($X, $Y + 5, $X + 65, $Y + 5);



$pdf->Ln(10);
$pdf->Cell(100);
$pdf->Cell(0, 5, $jabatanspesimen, 0, 0, 'C');
$pdf->Ln(30);
$pdf->Cell(100);
$pdf->writeHTML('<b>' . $namaspesimen . '</b>',  1, 0, 0, 0, 'C');
$pdf->Cell(100);
$pdf->Cell(0, 5, 'NIP. ' . $nipspesimen, 0, 0, 'C');
// -----------------------------------------------------------------------------


//============================================================+ KUITANSI
$pdf->AddPage();

//LOGO HEADER
$Y = $pdf->GetY();
$image_width = 18;
$pdf->Image('public/img/logo_garuda.png', 210 / 2 - ($image_width / 2), 7, $image_width, 0, 'PNG', '', 'R', false, 300, '', false, false, 0, false, false, false);

//HEADER
$pdf->ln(22);
$pdf->SetFont('times', 'B', $ukuranfontdefault);
$text = explode(PHP_EOL, $header);
$n = 1;
foreach ($text as $p) {
    $pdf->MultiCell(181, 0, $p, 0, 'C', 0, 1, '', '', true, '', true);
}

$pdf->SetY($Y);
$pdf->SetFont('helvetica', '', 6);
$pdf->Cell(120);
$pdf->Cell(0, 2, 'LAMPIRAN II', 0, 2, 'L');
$pdf->Cell(0, 2, 'PERATURAN MENTERI KEUANGAN REPUBLIK INDONESIA', 0, 2, 'L');
$pdf->Cell(0, 2, 'NOMOR : 113/PMK.05/2012', 0, 2, 'L');
$pdf->Cell(0, 2, 'TENTANG', 0, 2, 'L');
$pdf->Cell(0, 2, 'PERJALANAN DINAS JABATAN DALAM NEGERI PEJABAT', 0, 2, 'L');
$pdf->Cell(0, 2, 'NEGARA, PEGAWAI NEGERI, DAN PEGAWAI TIDAK TETAP', 0, 2, 'L');

$Y = $pdf->GetY();
$pdf->SetY($Y);
$pdf->ln(5);


$pdf->SetFont('helvetica', 'B', $ukuranfontdefault);
$pdf->ln(15);
$pdf->Write(0, 'KUITANSI', '', 0, 'C', true, 0, false, false, 0);
$pdf->ln(5);
$pdf->SetFont('', '', 9);

$pdf->Cell(30, 3, 'Sudah Terima dari ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, 'Kuasa Pengguna Anggaran Kantor Regional VIII Badan Kepegawaian Negara', 0, 1, 'L');

$pdf->Cell(30, 3, 'Uang Sebesar ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, '2.860.000', 0, 1, 'L');

$pdf->Cell(30, 3, 'Guna Pembayaran ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->MultiCell(0, 3, 'Biaya melakukan perjalanan dinas dalam rangka ' . $namakegiatan . ' bertempat di ' . $lokasikegiatan, 0, 'L');
$pdf->ln(2);

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Dari ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, 'Kepala Kantor Regional VIII BKN', 0, 1, 'L');

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Nomor ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $nomorst, 0, 1, 'L');

$pdf->Cell(40);
$pdf->Cell(30, 3, 'Tanggal ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $tanggalst, 0, 1, 'L');

$pdf->ln(2);
$pdf->Cell(30, 3, 'Nama ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, "$namapegawai", 0, 1, 'L');

$pdf->Cell(30, 3, 'Pangkat/Jabatan ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, "$pangkatpegawai / $golonganpegawai", 0, 2, 'L');
$pdf->Cell(0, 3, $jabatanpegawai, 0, 1, 'L');


$pdf->Cell(30, 3, 'Tempat Kedudukan ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, 'Banjarbaru', 0, 1, 'L');

$Y = $pdf->GetY();

$pdf->Cell(90);
$pdf->Cell(100, 3, 'Yang bepergian', 0, 1, 'C');
$pdf->Ln(12);
$pdf->Cell(90);
$pdf->MultiCell(100, 3, "<b>$namapegawai</b>",  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(90);
$pdf->Cell(100, 3, "NIP $nippegawai", 0, 0, 'C');

$pdf->SetY($Y);
$pdf->Ln(5);
$pdf->Ln(20);
$pdf->Cell(35, 3, 'Lampiran SPD Nomor ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $nomorspd, 0, 1, 'L');

$pdf->Cell(35, 3, 'Tanggal ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(0, 3, $tanggalsurat, 0, 1, 'L');




$pdf->SetFont('', 'B', 11);
$pdf->ln(5);
$pdf->Write(0, 'RINCIAN PERJALANAN DINAS', '', 0, 'C', true, 0, false, false, 0);
$pdf->ln(5);
$pdf->SetFont('', '', 9);
// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="2" border="1">
<tr>
    <th style="width:'5%;text-align:center;"><b>No.</b></th>
    <th style="width:'33%;" align="center"><b>Perincian Biaya</b></th>
    <th style="width:'38%;" align="center"><b>Jumlah</b></th>
    <th style="width:'26%;" align="center"><b>Keterangan</b></th>
</tr>
<tr>
    <td style="text-align:center;">1</td>
    <td align="left">Tiket Banjarmasin-Balikpapan</td>
    <td align="right">440.000</td>
    <td></td>
</tr>
<tr>
    <td style="text-align:center;">2</td>
    <td align="left">Tiket Balikpapan-Banjarmasin</td>
    <td align="right">540.000</td>
    <td></td>
</tr>
<tr>
    <td style="text-align:center;">3</td>
    <td align="left">Uang Harian (3 x 380.000)</td>
    <td align="right">1.140.000</td>
    <td></td>
</tr>
<tr>
    <td style="text-align:center;">1</td>
    <td align="left">Hotel (2 x 380.000)</td>
    <td align="right">740.000</td>
    <td></td>
</tr>
<tr>
    <td colspan="2" align="left"><b>Jumlah</b></td>
    <td align="right"><b>2.860.000</b></td>
    <td></td>
</tr>
<tr>
    <td colspan="4"> Terbilang : <b>Dua juta delapan ratu senam puluh ribu rupiah</b></td>
</tr>

</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Ln(0);
$pdf->Cell(130);
$pdf->Cell(0, 5, 'Banjarbaru, 30 November 2020', 0, 1, 'C');
$pdf->Ln(2);
$Y = $pdf->getY();



$pdf->setY($Y);
$pdf->Cell(100, 3, 'Telah dibayar sejumlah', 0, 1, 'C');
$pdf->MultiCell(100, 3, '<b>Rp. 2.860.000</b>',  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(100, 3, 'Bendahara Pengeluaran,', 0, 0, 'C');
$pdf->Ln(15);
$pdf->MultiCell(100, 3, '<b>SRIPAH</b>',  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(100, 3, 'NIP 198207122006042001', 0, 0, 'C');



$pdf->setY($Y);
$pdf->Cell(80);
$pdf->Cell(100, 3, 'Telah menerima jumlah uang sebesar', 0, 1, 'C');
$pdf->Cell(80);
$pdf->MultiCell(100, 3, '<b>Rp. 2.860.000</b>',  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(80);
$pdf->Cell(100, 3, 'Yang menerima,', 0, 0, 'C');
$pdf->Ln(15);
$pdf->Cell(80);
$pdf->MultiCell(100, 3, '<b>HOSPITA GLORIA SITUMORANG, S.H., M.AP</b>',  0, 'C', 0, 1, '', '', 1, 0, 1);
$pdf->Cell(80);
$pdf->Cell(100, 3, 'NIP 197401302005022001', 0, 0, 'C');

$Y = $pdf->getY();

$pdf->Ln(2);
$pdf->Line(5, $Y + 10, 210 - 5, $Y + 10);
$pdf->Ln(15);


$pdf->SetFont('', 'B', 11);
$pdf->Write(0, 'PERHITUNGAN SPD RAMPUNG', '', 0, 'C', true, 0, false, false, 0);
$pdf->SetFont('', '', 9);

$pdf->Ln(5);

$Yn = $pdf->getY();
$pdf->Cell(40, 3, 'Ditetapkan sejumlah ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, '2.860.000', 0, 1, 'R');

$pdf->Cell(40, 3, 'Yang telah dibayar semula ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, '-', 0, 1, 'R');

$Y = $pdf->getY();
$pdf->Line(60, $Y, 87, $Y);

$pdf->SetFont('', 'B', 9);
$pdf->Cell(40, 3, 'Sisa kurang/lebih ', 0, 0, 'L');
$pdf->Cell(4, 3, ':', 0, 0, 'L');
$pdf->Cell(4, 3, 'Rp', 0, 0, 'L');
$pdf->Cell(25, 3, '2.860.000', 0, 1, 'R');
$pdf->SetFont('', '', 9);


$pdf->setY($Yn);
$pdf->Cell(100);
$pdf->Cell(0, 3, $jabatanspesimen, 0, 0, 'C');
$pdf->Cell(100);
$pdf->Cell(0, 3, 'Kantor Regional VIII BKN', 0, 0, 'C');
$pdf->Ln(15);
$pdf->Cell(100);
$pdf->writeHTML('<b>' . $namaspesimen . '</b>',  1, 0, 0, 0, 'C');
$pdf->Cell(100);
$pdf->Cell(0, 3, 'NIP. ' . $nipspesimen, 0, 0, 'C');
// -----------------------------------------------------------------------------


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_003.pdf', 'I');
exit();

//============================================================+
// END OF FILE
//============================================================+