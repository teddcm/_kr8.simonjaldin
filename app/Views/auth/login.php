<!DOCTYPE html>
<!--
Template Name: Metronic - Bootstrap 4 HTML, React, Angular 11 & VueJS Admin Dashboard Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: https://1.envato.market/EA4JP
Renew Support: https://1.envato.market/EA4JP
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->

<head>
  <base href="../../../">
  <meta charset="utf-8" />
  <title>Login</title>
  <meta name="description" content="Login page example" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="canonical" href="https://keenthemes.com/metronic" />
  <!--begin::Fonts-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
  <!--end::Fonts-->
  <!--begin::Page Custom Styles(used by this page)-->
  <link href="<?php echo base_url(); ?>/public/metronic/assets/css/pages/login/login-2.css" rel="stylesheet" type="text/css" />
  <!--end::Page Custom Styles-->
  <!--begin::Global Theme Styles(used by all pages)-->
  <link href="<?php echo base_url(); ?>/public/metronic/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>/public/metronic/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
  <!--end::Global Theme Styles-->
  <!--begin::Layout Themes(used by all pages)-->
  <!--end::Layout Themes-->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>/public/metronic/assets/media/logos/favicon.ico" />
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled page-loading">
  <!--begin::Main-->
  <div class="d-flex flex-column flex-root">
    <!--begin::Login-->
    <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
      <!--begin::Aside-->
      <div class="login-aside order-2 order-lg-1 d-flex flex-row-auto position-relative overflow-hidden" style="background-color: white;">
        <!--begin: Aside Container-->
        <div class="d-flex flex-column-fluid flex-column justify-content-between py-9 px-7 py-lg-13 px-lg-35">
          <!--begin::Logo-->
          <!-- <a href="#" class="text-center pt-2">
            <img src="<?php echo base_url(); ?>/public/metronic/assets/media/logos/logo.png" class="max-h-75px" alt="" />
          </a> -->
          <!--end::Logo-->
          <!--begin::Aside body-->
          <div class="d-flex flex-column-fluid flex-column flex-center">
            <!--begin::Signin-->
            <div class="login-form login-signin py-11">
              <!--begin::Form-->
              <?php echo form_open('auth/signin'); ?>
              <!-- <form class="form" novalidate="novalidate" id="kt_login_signin_form"> -->
              <!--begin::Title-->
              <div class="text-center pb-8">
                <h2 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg"><?php echo lang('Auth.login_heading'); ?></h2>
                <?php if (@$message) { ?>
                  <style>
                    ul {
                      list-style-type: none;
                      padding: 0;
                      margin: 0;
                    }
                  </style>
                  <?php echo $message; ?>
                <?php } ?>
              </div>
              <!--end::Title-->
              <!--begin::Form group-->
              <div class="form-group">
                <label class="font-size-h6 font-weight-bolder text-dark"><?php echo form_label(lang('Auth.login_identity_label'), 'identity'); ?></label>
                <?php echo form_input($identity); ?>
              </div>
              <!--end::Form group-->
              <!--begin::Form group-->
              <div class="form-group">
                <div class="d-flex justify-content-between mt-n5">
                  <label class="font-size-h6 font-weight-bolder text-dark pt-5"><?php echo form_label(lang('Auth.login_password_label'), 'password'); ?></label>
                  <a href="javascript:;" tabindex="-1" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot"><?php echo lang('Auth.login_forgot_password'); ?></a>
                </div>
                <?php echo form_input($password); ?>
              </div>
              <div class="form-group">
                <div class="checkbox-inline">
                  <label class="checkbox checkbox-square">
                    <?php echo form_checkbox('remember', '1', false, 'id="remember"'); ?>
                    <span></span>Ingat Saya
                  </label>

                </div>
              </div>
              <!--end::Form group-->
              <!--begin::Action-->
              <div class="text-center pt-2">
                <?php echo form_submit('submit', lang('Auth.login_submit_btn'), "class='btn btn-dark font-weight-bolder font-size-h6 px-8 py-4 my-3' id='kt_login_signin_submit'"); ?>
              </div>
              <!--end::Action-->
              <?php echo form_close(); ?>
              <!--end::Form-->
            </div>
            <!--end::Signin-->
          </div>
          <!--end::Aside body-->
        </div>
        <!--end: Aside Container-->
      </div>
      <!--begin::Aside-->
      <!--begin::Content-->
      <div class="content order-1 order-lg-2 d-flex flex-column w-100 pb-0" style="background-color: #B1DCED;background-image: url(<?php echo base_url(); ?>/public/metronic/assets/media/svg/illustrations/login-visual-2.svg);">
        <!--begin::Title-->
        <div class="d-flex flex-column justify-content-center text-center pt-lg-0 pt-md-0 pt-sm-0 px-lg-0 pt-0 px-7">
          <h3 class="display4 font-weight-bolder my-7 text-dark" style="color: #986923;">Simonjaldin</h3>
          <p class="font-weight-bolder font-size-h2-md font-size-lg text-dark opacity-70">Sistem Informasi
            <br />Monitoring Perjalanan Dinas</p>
        </div>
        <!--end::Title-->
        <!--begin::Image-->
        <!--end::Image-->
      </div>
      <!--end::Content-->
    </div>
    <!--end::Login-->
  </div>
  <!--end::Main-->

  <!--begin::Global Config(global config for global JS scripts)-->
  <script>
    var KTAppSettings = {
      "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1200
      },
      "colors": {
        "theme": {
          "base": {
            "white": "#ffffff",
            "primary": "#8950FC",
            "secondary": "#E5EAEE",
            "success": "#1BC5BD",
            "info": "#8950FC",
            "warning": "#FFA800",
            "danger": "#F64E60",
            "light": "#F3F6F9",
            "dark": "#212121"
          },
          "light": {
            "white": "#ffffff",
            "primary": "#E1E9FF",
            "secondary": "#ECF0F3",
            "success": "#C9F7F5",
            "info": "#EEE5FF",
            "warning": "#FFF4DE",
            "danger": "#FFE2E5",
            "light": "#F3F6F9",
            "dark": "#D6D6E0"
          },
          "inverse": {
            "white": "#ffffff",
            "primary": "#ffffff",
            "secondary": "#212121",
            "success": "#ffffff",
            "info": "#ffffff",
            "warning": "#ffffff",
            "danger": "#ffffff",
            "light": "#464E5F",
            "dark": "#ffffff"
          }
        },
        "gray": {
          "gray-100": "#F3F6F9",
          "gray-200": "#ECF0F3",
          "gray-300": "#E5EAEE",
          "gray-400": "#D6D6E0",
          "gray-500": "#B5B5C3",
          "gray-600": "#80808F",
          "gray-700": "#464E5F",
          "gray-800": "#1B283F",
          "gray-900": "#212121"
        }
      },
      "font-family": "Poppins"
    };
  </script>
  <!--end::Global Config-->
  <!--begin::Global Theme Bundle(used by all pages)-->
  <script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/global/plugins.bundle.js"></script>
  <script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
  <script src="<?php echo base_url(); ?>/public/metronic/assets/js/scripts.bundle.js"></script>
  <!--end::Global Theme Bundle-->
</body>
<!--end::Body-->

</html>