<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label">Pengguna Aplikasi</h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                <span class="svg-icon svg-icon-sm">
                    <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo4\dist/../src/media/svg/icons\Communication\Add-user.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24" />
                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                        </g>
                    </svg>
                    <!--end::Svg Icon--></span>
                Tambah Pengguna
            </button>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <!-- <div class="table-responsive"> -->
        <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th style="width: 20%;">Profil</th>
                    <th style="width: 15%;">Kontak</th>
                    <th style="width: 10%;">Status</th>
                    <th>Grup</th>
                    <th style="width: 10%;">Login Terakhir</th>
                    <th style="width: 10%;">Aksi</th>
                </tr>
            </thead>
        </table>
        <!-- </div> -->
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->

<!-- Modal-->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="myForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Username</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-user-tag text-success"></i>
                                            </span>
                                        </div>
                                        <input name="username" type="text" class="form-control form-control-sm" placeholder="Masukkan Username" />
                                        <input name="id" type="hidden" value="0" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Nama Lengkap</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-id-card text-success"></i>
                                            </span>
                                        </div>
                                        <input name="first_name" type="text" class="form-control form-control-sm" placeholder="Masukkan Nama Lengkap" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Email</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-envelope text-success"></i>
                                            </span>
                                        </div>
                                        <input name="email" type="text" class="form-control form-control-sm" placeholder="Masukkan email" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Nomor HP</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-phone text-success"></i>
                                            </span>
                                        </div>
                                        <input name="phone" type="text" class="form-control form-control-sm" placeholder="Masukkan Nomor HP" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Password</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-key text-success"></i>
                                            </span>
                                        </div>
                                        <input name="password" type="text" class="form-control form-control-sm" placeholder="**********" style="-webkit-text-security: disc;" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-5 col-form-label">Ulangi Password</label>
                                <div class="col-lg-7">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-key text-success"></i>
                                            </span>
                                        </div>
                                        <input name="password_confirm" type="text" class="form-control form-control-sm" placeholder="**********" style="-webkit-text-security: disc;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offset-lg-2 col-lg-3" id="grup_checkbox" style="display: none;">
                            <div class="form-group">
                                <label>Grup</label>
                                <div class="checkbox-list">
                                    <?php foreach ($groups as $group) : ?>
                                        <label class="checkbox" data-toggle="tooltip" data-placement="left" data-container="body" data-boundary="window" title="<?php echo $group['description']; ?>">
                                            <?php
                                            $gID = $group['id'];
                                            $checked = null;
                                            $item = null;

                                            ?>
                                            <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>" <?php echo $checked; ?>>
                                            <span></span>
                                            <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                        </label>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold" id="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require('app/Views/index/i-js-global.php'); ?>

<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script>
    $('#myForm').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '<?php echo base_url() . '/auth/simpan_user/'; ?>' + $('input[name ="id"]').val(),
            data: $('#myForm').serialize(),
            success: function(result) {
                var data = $.parseJSON(result);
                jsonMessageAlert(data['result'], data['message']);
                if (data['result'] == 'success') {
                    $("#staticBackdrop").modal('hide');

                }
            }
        });


    });
    $(document).ready(function() {


        var oTable = $('#kt_datatable').DataTable({
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url(); ?>/api/datatables/user",
            columns: [{
                    data: 'username'
                },
                {
                    data: 'email'
                },
                {
                    data: 'active'
                },
                {
                    data: 'active'
                },
                {
                    data: 'last_login'
                },
                {
                    data: 'id',
                    responsivePriority: -1
                },
            ],
            columnDefs: [{
                    targets: 0,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                        <span class="btn py-0 font-weight-bold btn-outline-primary btn-square btn-sm">' + full.username + '</span>\
                        <br><b>' + full.first_name + '</b>\
						';
                    },
                }, {
                    targets: 1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                        ' + full.email + '\
                        <br>' + full.phone + '\
						';
                    },
                }, {
                    targets: 2,
                    render: function(data, type, full, meta) {
                        var status = {
                            1: {
                                'title': 'aktif',
                                'class': ' btn-outline-success deaktivasi'
                            },
                            0: {
                                'title': 'nonaktif',
                                'class': ' btn-outline-danger aktivasi'
                            },
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<button id="' + full.id + '" class=" btn-square py-0 btn font-weight-bold' + status[data].class + '">' + status[data].title + '</button>';
                    },
                }, {
                    targets: 3,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        var string = '';
                        $.each(full.groups, function(key, val) {
                            string += '<span class="label label-sm font-weight-bold  label-inline label-primary">' + val['name'] + '</span> ';
                        });

                        return '\
							<span>' + string + '</span>\
						';
                    },
                }, {
                    targets: -1,
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon edit" title="Edit"  >\
								<i class="la la-edit"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon hapus" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						';
                    },
                },

            ],
        });

        $('#kt_datatable tbody').on('click', '.edit', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            // alert(rowData.username);
            $('#staticBackdrop').modal('show');
            $('#grup_checkbox').show();
            $('input[name ="id"]').val(rowData.id);
            $('input[name ="username"]').val(rowData.username).prop("readonly", true);;
            $('input[name ="first_name"]').val(rowData.first_name);
            $('input[name ="email"]').val(rowData.email);
            $('input[name ="phone"]').val(rowData.phone);
            $.each(rowData.groups, function(key, val) {
                console.log(val.id);
                $('input[name ="groups[]"][value="' + val.id + '"]').prop("checked", true);;
            });

        });

        $('#kt_datatable tbody').on('click', '.deaktivasi', function() {
            var id = $(this).attr("id");
            Swal.fire({
                title: "Apakah anda akan menonaktifkan pengguna ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/auth/deaktivasi/'; ?>' + id,
                        data: {
                            id: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                oTable.ajax.reload();
                            }
                        }
                    });
                }
            });
        });
        $('#kt_datatable tbody').on('click', '.aktivasi', function() {
            var id = $(this).attr("id");
            Swal.fire({
                title: "Apakah anda akan mengaktifkan pengguna ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/auth/aktivasi/'; ?>' + id,
                        data: {
                            id: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                oTable.ajax.reload();
                            }
                        }
                    });
                }
            });
        });
        $('#kt_datatable tbody').on('click', '.hapus', function() {

            var rowData = oTable.row($(this).closest("tr")).data();
            var id = rowData.id;
            Swal.fire({
                title: "Apakah anda akan menghapus pengguna ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Hapus!",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        title: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Baik",
                        cancelButtonText: "Batal"
                    }).then(function(result2) {
                        if (result2.value) {
                            $.ajax({
                                type: 'post',
                                url: '<?php echo base_url() . '/auth/hapus/'; ?>' + id,
                                data: {
                                    id: id,
                                },
                                success: function(result) {
                                    var data = $.parseJSON(result);
                                    jsonMessageAlert(data['result'], data['message']);
                                    if (data['result'] == 'success') {
                                        oTable.ajax.reload();
                                        if (data['option'] == 'reload') {
                                            setTimeout(function() {
                                                window.location.href = '<?php echo base_url() . '/auth/signout'; ?>';
                                            }, 4000);

                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });
        });

        $("#staticBackdrop").on('hidden.bs.modal', function() {
            $('#grup_checkbox').hide();
            $("#myForm")[0].reset();
            oTable.ajax.reload();
            $('input[name ="id"]').val(0);
            $("#staticBackdrop :input").prop("readonly", false);
            $('input[name ="groups[]"]').prop("checked", false);;

        });
    });
</script>
<!--end::Page Scripts-->