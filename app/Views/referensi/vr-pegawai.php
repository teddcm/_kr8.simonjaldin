<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label"><?php echo $page . ' ' . $_SESSION['tokenizer']; ?></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myFormModal">
                <i class="fas fa-folder-plus"></i>
                Tambah/Update Pegawai
            </button>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <!-- <div class="table-responsive"> -->
        <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th style="width: 15%;">NIP</th>
                    <th style="width: 25%">Nama</th>
                    <th style="width: 25%;">Email</th>
                    <th style="width: 25%;">No. HP</th>
                    <th style="width: 10%;">Aksi</th>
                </tr>
            </thead>
        </table>
        <!-- </div> -->
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->

<!-- Modal Tambah NIP-->
<div class="modal fade" id="myFormModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form" id="myForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-1">
                                <label for="exampleTextarea">Masukkan NIP
                                    <span class="text-danger">*</span>
                                </label>
                                <textarea class="form-control" name="nips" id="nips" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 104px;"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo view('index/i-js-global'); ?>
<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script>
    $(document).ready(function() {
        var oTable = $('#kt_datatable').DataTable({
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "ajax": "<?php echo base_url(); ?>/api/datatables/ref_pegawai",
            columns: [{
                    data: 'nipBaru',
                    className: 'align-top'
                },
                {
                    data: 'nama',
                    className: 'align-top'
                },
                {
                    data: 'email',
                    className: 'align-top'
                },
                {
                    data: 'noHp',
                    className: 'align-top'
                }, {
                    data: 'id',
                    className: 'align-top'
                },
            ],
            columnDefs: [],
        });




        $('#kt_datatable tbody').on('click', '.hapusTujuan', function() {
            var id = $(this).attr('id_tujuan');
            Swal.fire({
                title: "Akan menghapus tujuan ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Baik",
                cancelButtonText: "Batal"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . '/perjadin/st/hapusTujuan/'; ?>' + id,
                        data: {
                            id_tujuan: id,
                        },
                        success: function(result) {
                            var data = $.parseJSON(result);
                            jsonMessageAlert(data['result'], data['message']);
                            if (data['result'] == 'success') {
                                oTable.ajax.reload();
                            }
                        }
                    });
                }
            });
        });
        $('#kt_datatable tbody').on('click', '.hapus', function() {

            var rowData = oTable.row($(this).closest("tr")).data();
            var id = rowData.id_st;
            Swal.fire({
                title: "Apakah anda akan menghapus data ini?",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: "Hapus!",
                cancelButtonText: "Batal"
            }).then(function(resultx) {
                if (resultx.value) {
                    Swal.fire({
                        title: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Baik",
                        cancelButtonText: "Batal"
                    }).then(function(result) {
                        if (result.value) {
                            $.ajax({
                                type: 'post',
                                url: '<?php echo base_url() . '/perjadin/st/hapusST/'; ?>' + id,
                                data: {
                                    id_st: id,
                                },
                                success: function(result) {
                                    var data = $.parseJSON(result);
                                    jsonMessageAlert(data['result'], data['message']);
                                    if (data['result'] == 'success') {
                                        oTable.ajax.reload();
                                    }
                                }
                            });
                        }
                    });
                }
            });
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#myForm').on('submit', function(e) {
            e.preventDefault();
            // alert($('#nips').val());
            var nips = $('#nips').val().split(",");
            var i;
            for (i = 0; i < nips.length; ++i) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . '/referensi/pegawai/getDatapegawai/'; ?>' + nips[i],
                    data: {
                        id: nips[i],
                    },
                    success: function(result) {
                        var data = $.parseJSON(result);
                        jsonMessageAlert(data['result'], data['message']);
                        if (data['result'] == 'success') {
                            $("#myFormModal").modal('hide');
                            oTable.ajax.reload();
                        }
                    }
                });
                // console.log(nips[i]);
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $("#myFormModal").on('hidden.bs.modal', function() {
            $("#myForm")[0].reset();
            // oTable.ajax.reload();
            $('input[name ="id_st_1"]').val(0);

        });
    });
</script>
<!--end::Page Scripts-->
<?php echo view('index/0-bottom'); ?>