<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header">
        <div class="card-title">
            <span class="card-icon">
                <i class="fas fa-users-cog text-primary"></i>
            </span>
            <h3 class="card-label"><?php echo $page; ?></h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <button type="button" id="tambahMAK" class="btn btn-primary">
                <i class="fas fa-folder-plus"></i>
                Tambah MAK
            </button>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <!-- <div class="table-responsive"> -->
        <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
            <thead>
                <tr>
                    <th style="width: 15%;">Kode</th>
                    <th style="width: 55%">Detail</th>
                    <th>Tahun</th>
                    <th>Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
        <!-- </div> -->
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->

<div class="modal fade" id="myFormModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form" id="myForm">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group mb-1">
                                <label for="exampleTextarea">Kode
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="kode_mak" id="kode_mak">
                                <input type="hidden" class="form-control" name="id_mak" id="id_mak" value="0">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group mb-1">
                                <label for="exampleTextarea">Tahun
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="tahun_mak" id="tahun_mak">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-group mb-1">
                                <label for="exampleTextarea">Status
                                    <span class="text-danger">*</span>
                                </label>
                                <select class="form-control" name="aktif" id="aktif">
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mb-1">
                                <label for="exampleTextarea">Detail
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="detail_mak" id="detail_mak">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo view('index/i-js-global'); ?>
<!--begin::Page Vendors(used by this page)-->
<script src="<?php echo base_url(); ?>/public/metronic/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<!--end::Page Vendors-->
<!--begin::Page Scripts(used by this page)-->
<script>
    $(document).ready(function() {
        var oTable = $('#kt_datatable').DataTable({
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "ajax": "<?php echo base_url(); ?>/api/datatables/ref_mak",
            columns: [{
                    data: 'kode_mak',
                    className: 'align-top'
                },
                {
                    data: 'detail_mak',
                    className: 'align-top'
                },
                {
                    data: 'tahun_mak',
                    className: 'align-top'
                }, {
                    data: 'aktif',
                    className: 'align-top'
                }, {
                    data: 'id_mak',
                    className: 'text-center'
                },
            ],

            columnDefs: [{
                targets: -1,
                orderable: false,
                render: function(data, type, full, meta) {
                    return '\
                    ' + ('<button class="btn btn-sm btn-icon btn-circle btn-twitter btn-light-warning editMAK"><i class="la la-edit"></i></button>') + '\
                    ' + ('<button class="btn btn-sm btn-icon btn-circle btn-danger btn-light-warning hapusMAK"><i class="la la-trash"></i></button>') + '\
						';
                },
            }, {
                targets: -2,
                orderable: false,
                render: function(data, type, full, meta) {
                    return '\
                    ' + (data == '1' ? '<i class="fas fa-check text-success" title="Aktif"></i>' : '<i class="fas fa-times text-danger" title="Tidak Aktif"></i>') + '\
						';
                },
            }]
        });

        $('#tambahMAK').on('click', function() {
            $('#myFormModal').modal('show');
            $('#kode_mak').val('');
            $('#detail_mak').val('');
            $('#tahun_mak').val(<?php echo date('Y'); ?>);
            $('#id_mak').val(0);
        });

        $('#kt_datatable tbody').on('click', '.editMAK', function() {
            var rowData = oTable.row($(this).closest("tr")).data();
            $('#myFormModal').modal('show');
            $('#kode_mak').val(rowData.kode_mak);
            $('#detail_mak').val(rowData.detail_mak);
            $('#tahun_mak').val(rowData.tahun_mak);
            $('#id_mak').val(rowData.id_mak);
            $('#aktif').val(rowData.aktif);

        });


        $('#kt_datatable tbody').on('click', '.hapusMAK', function() {

            var rowData = oTable.row($(this).closest("tr")).data();
            var id = rowData.id_mak;
            Swal.fire({
                title: "Untuk kepentingan historis, data MAK tidak dapat dihapus. \n\n Silakan gunakan fitur \n(status:tidak aktif).",
                icon: "info",
                showCancelButton: false,
                cancelButtonText: "OK"
            })

            // Swal.fire({
            //     title: "Apakah anda akan menghapus data ini?",
            //     icon: "question",
            //     showCancelButton: true,
            //     confirmButtonText: "Hapus!",
            //     cancelButtonText: "Batal"
            // }).then(function(resultx) {
            //     if (resultx.value) {
            //         Swal.fire({
            //             title: "Data yang dihapus TIDAK DAPAT dikembalikan lagi ",
            //             icon: "warning",
            //             showCancelButton: true,
            //             confirmButtonText: "Baik",
            //             cancelButtonText: "Batal"
            //         }).then(function(result) {
            //             if (result.value) {
            //                 $.ajax({
            //                     type: 'post',
            //                     url: '<?php echo base_url() . '/perjadin/st/hapusST/'; ?>' + id,
            //                     data: {
            //                         id_st: id,
            //                     },
            //                     success: function(result) {
            //                         var data = $.parseJSON(result);
            //                         jsonMessageAlert(data['result'], data['message']);
            //                         if (data['result'] == 'success') {
            //                             oTable.ajax.reload();
            //                         }
            //                     }
            //                 });
            //             }
            //         });
            //     }
            // });
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#myForm').on('submit', function(e) {
            e.preventDefault();
            // alert($('#nips').val());
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . '/referensi/mak/simpan'; ?>',
                data: $('#myForm').serialize(),
                success: function(result) {
                    var data = $.parseJSON(result);
                    jsonMessageAlert(data['result'], data['message']);
                    if (data['result'] == 'success') {
                        $("#myFormModal").modal('hide');
                        oTable.ajax.reload();
                    }
                }
            });
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $("#myFormModal").on('hidden.bs.modal', function() {
            $("#myForm")[0].reset();
            // oTable.ajax.reload();
            $('input[name ="id_st_1"]').val(0);

        });
    });
</script>
<!--end::Page Scripts-->
<?php echo view('index/0-bottom'); ?>