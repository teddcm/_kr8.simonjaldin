<?php
function json_message($result, $message = null, $option = null)
{
    $data['result'] = $result;
    $data['message'] = $message;
    $data['option'] = $option;
    echo json_encode($data);
    // print_r($data);
}

function getDatesFromRange($start, $end, $format = 'Y-m-d')
{
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach ($period as $date) {
        $array[] = $date->format($format);
    }

    return $array;
}

function tulisGelar($nama, $gelar_depan = null, $gelar_belakang = null)
{
    $nama = $nama;
    if ($gelar_depan != null & $gelar_depan != '') {
        $nama = $gelar_depan . ' ' . $nama;
    }
    if ($gelar_belakang != null & $gelar_belakang != '') {
        $nama = $nama . ', ' . $gelar_belakang;
    }
    return $nama;
}
function uuid($slash = false)
{
    $string = $slash ? '%04x%04x-%04x-%04x-%04x-%04x%04x%04x' : '%04x%04x%04x%04x%04x%04x%04x%04x';
    return sprintf(
        $string,
        // 32 bits for "time_low"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff)
    );
}

function buatNomorSurat($no_urut, $tanggal = null, $format_st = "%no%/ST/K/KR.VIII/%bln%/%thn%", $no_urut_length = 3)
{
    if ($tanggal == null) {
        $tanggal = (date('d-m-Y'));
    }
    $format_st = str_replace("%no%", str_pad($no_urut, $no_urut_length, '0', STR_PAD_LEFT), $format_st);
    $format_st = str_replace("%bln%", numberToRoman(date_format(date_create($tanggal), "n")), $format_st);
    $format_st = str_replace("%thn%", (date_format(date_create($tanggal), "Y")), $format_st);
    return $format_st;
}

function numberToRoman($num)
{
    $n = intval($num);
    $result = '';
    $lookup = array(
        'M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
        'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
        'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1
    );
    foreach ($lookup as $roman => $value) {
        $matches = intval($n / $value);
        $result .= str_repeat($roman, $matches);
        $n = $n % $value;
    }
    return $result;
}

function reverseDate($date)
{ //membalik format YYYY-MM-DD ke DD-MM-YYY dan sebaliknya
    $date = substr($date, 0, 10);
    $date = str_replace(' ', '', $date);
    if ($date != '') {
        $newdate = explode('-', $date);
        return $newdate[2] . '-' . $newdate[1] . '-' . $newdate[0];
    }
}

function convertDate($date, $format = 'd F Y', $bahasa = "id")
{
    $en = array(
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tueday", "Wedday", "Thursday", "Friday", "Saturday",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    );

    $id = array(
        "Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab",
        "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu",
        "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember",
        "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"
    );
    if (is_array($date)) {
        $a = $date[0];
        $b = $date[1];
        if ($a == $b) {
            return str_replace($en, $$bahasa, date_format(date_create($a), $format));
        } else if (date_format(date_create($a), 'F Y') == date_format(date_create($b), 'F Y')) {
            return str_replace($en, $$bahasa, date_format(date_create($a), 'd')) . ' s.d. ' . str_replace($en, $$bahasa, date_format(date_create($b), 'd F Y'));
        } else if (date_format(date_create($a), 'Y') == date_format(date_create($b), 'Y')) {
            return str_replace($en, $$bahasa, date_format(date_create($a), 'd F')) . ' s.d. ' . str_replace($en, $$bahasa, date_format(date_create($b), 'd F Y'));
        } else {
            return str_replace($en, $$bahasa, date_format(date_create($a), 'd F Y')) . ' s.d. ' . str_replace($en, $$bahasa, date_format(date_create($b), 'd F Y'));
        }
    } else {
        return str_replace($en, $$bahasa, date_format(date_create($date), $format));
    }
}

function arraytoupper($input, $case = CASE_UPPER)
{
    $narray = array();
    if (!is_array($input)) {
        return $narray;
    }
    foreach ($input as $key => $value) {
        if (is_array($value)) {
            $narray[$key] = arraytoupper($value, $case);
            continue;
        }
        $narray[$key] = strtoupper($value);
    }
    return $narray;
}


function terbilang_rupiah($x)
{
    $terbilang = terbilang($x);
    return trim($terbilang . " rupiah");
}

function angka_rupiah($x, $prefix = 'Rp ')
{
    return $prefix . number_format($x, "0", ",", ".") . ",-";
}

function terbilang($x, $first = false)
{
    $ambil = array(
        "", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh",
        "delapan", "sembilan", "sepuluh", "sebelas"
    );
    if ($x <= 0) {
        return $first == false ? 'nol' : '';
    } elseif ($x < 12) {
        return " " . $ambil[$x];
    } elseif ($x < 20) {
        return terbilang($x - 10, true) . " belas";
    } elseif ($x < 100) {
        return terbilang($x / 10, true) . " puluh" . terbilang($x % 10, true);
    } elseif ($x < 200) {
        return " seratus" . terbilang($x - 100, true);
    } elseif ($x < 1000) {
        return terbilang($x / 100, true) . " ratus" . terbilang($x % 100, true);
    } elseif ($x < 2000) {
        return " seribu" . terbilang($x - 1000, true);
    } elseif ($x < 1000000) {
        return terbilang($x / 1000, true) . " ribu" . terbilang($x % 1000, true);
    } elseif ($x < 1000000000) {
        return terbilang($x / 1000000, true) . " juta" . terbilang($x % 1000000, true);
    }
}

function convertRomawi($n)
{
    $result = "";
    $iromawi = array(
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
        20 => "XX", 30 => "XXX", 40 => "XL", 50 => "L", 60 => "LX",
        70 => "LXX", 80 => "LXXX", 90 => "XC", 100 => "C", 200 => "CC",
        300 => "CCC", 400 => "CD", 500 => "D", 600 => "DC",
        700 => "DCC", 800 => "DCCC",
        900 => "CM", 1000 => "M",
        2000 => "MM", 3000 => "MMM"
    );
    if (array_key_exists($n, $iromawi)) {
        $result = $iromawi[$n];
    } elseif ($n >= 11 && $n <= 99) {
        $i = $n % 10;
        $result = $iromawi[$n - $i] . convertRomawi($n % 10);
    } elseif ($n >= 101 && $n <= 999) {
        $i = $n % 100;
        $result = $iromawi[$n - $i] . convertRomawi($n % 100);
    } else {
        $i = $n % 1000;
        $result = $iromawi[$n - $i] . convertRomawi($n % 1000);
    }
    return $result;
}



function isValidDate($date, $separator)
{ //untuk DD-MM-YYYY
    if (count(explode($separator, $date)) == 3) {
        $pattern = "/^([0-9]{2})" . $separator . "([0-9]{2})" . $separator . "([0-9]{4})$/";
        if (preg_match($pattern, $date, $parts)) {
            //echo $parts[1]. $parts[2]. $parts[3];
            if (checkdate($parts[2], $parts[1], $parts[3]))
                return true;
            /* This is a valid date */
            else
                return false;
            /* This is an invalid date */
        } else {
            return false;
            /* This is an invalid date in terms of format */
        }
    } else {
        return false;
        /* Day, Month, Year - either of them not present */
    }
}


function get_timeago($ptime)
{
    $estimate_time = time() - $ptime;

    if ($estimate_time < 1) {
        return 'baru saja';
    }

    $condition = array(
        12 * 30 * 24 * 60 * 60 => 'tahun',
        30 * 24 * 60 * 60 => 'bulan',
        24 * 60 * 60 => 'hari',
        60 * 60 => 'jam',
        60 => 'menit',
        1 => 'detik'
    );

    foreach ($condition as $secs => $str) {
        $d = $estimate_time / $secs;

        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . $str . ' yang lalu';
        }
    }
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
