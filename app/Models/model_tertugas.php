<?php

namespace App\Models;

use CodeIgniter\Model;

class model_tertugas extends Model
{
    protected $table      = 'data_tertugas';
    protected $primaryKey = 'id_tertugas';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_tertugas', 'nip_pegawai', 'id_tujuan', 'id_st',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
