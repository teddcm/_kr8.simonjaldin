<?php

namespace App\Models;

use CodeIgniter\Model;

class model_log extends Model
{
    protected $table      = 'log';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['log_user','log_jenis','log_detail'];

    protected $useTimestamps = true;
    protected $createdField  = 'log_datetime';
    protected $updatedField  = 'log_datetime';
    protected $deletedField  = 'log_datetime';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
