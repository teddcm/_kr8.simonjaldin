<?php

namespace App\Models;

use CodeIgniter\Model;

class model_referensi_spd extends Model
{
    protected $table      = 'referensi_spd';
    protected $primaryKey = 'id_spd';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_spd'];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
