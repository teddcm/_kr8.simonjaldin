<?php

namespace App\Models;

use CodeIgniter\Model;

class model_kabkota extends Model
{
    protected $table      = 'tabel_kabkota';
    protected $primaryKey = 'id_kabkota';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_kabkota', 'nama_kabkota', 'id_prov',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
