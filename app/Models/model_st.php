<?php

namespace App\Models;

use CodeIgniter\Model;

class model_st extends Model
{
    protected $table      = 'data_st';
    protected $primaryKey = 'id_st';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_st', 'no_st', 'tgl_st', 'urutan_st', 'spesimen_st', 'kegiatan_st', 'creator', 'creator'];

    protected $useTimestamps = true;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;


    public function getLastNoSt($tahun = null)
    {
        $tahun = $tahun == null ? date('Y') : $tahun;

        $db      = \Config\Database::connect();
        $builder = $db->table('data_st');

        $builder->where('YEAR(tgl_st)', $tahun);
        $builder->where('deleted IS NULL', null, false);
        $builder->selectMax('urutan_st');
        $numRow = $builder->countAllResults(false);
        $query = $builder->get();

        if ($numRow > 0) {
            return $query->getRow()->urutan_st;
        } else {
            return 0;
        }
    }
}
