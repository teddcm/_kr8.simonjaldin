<?php

namespace App\Models;

use CodeIgniter\Model;

class model_mak extends Model
{
    protected $table      = 'tabel_mak';
    protected $primaryKey = 'id_mak';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_mak', 'nama_akun', 'tahun_mak', 'aktif',];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
