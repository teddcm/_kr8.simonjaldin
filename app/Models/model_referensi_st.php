<?php

namespace App\Models;

use CodeIgniter\Model;

class model_referensi_st extends Model
{
    protected $table      = 'referensi_st';
    protected $primaryKey = 'id_st';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_st'];

    protected $useTimestamps = false;
    protected $createdField  = 'created';
    protected $updatedField  = 'updated';
    protected $deletedField  = 'deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;
}
