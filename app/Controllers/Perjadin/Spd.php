<?php

namespace App\Controllers\perjadin;

use App\Controllers\BaseController;
use App\Models\model_kabkota;
use App\Models\model_mak;
use App\Models\model_biaya;
use App\Models\model_spd;
use App\Models\model_referensi_spd;

class Spd extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);
    }


    public function index()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url('auth/signin'));
        }

        $model_kabkota = new model_kabkota();
        $kabkota = $model_kabkota
            ->asObject()
            ->select('tabel_prov.nama_prov,tabel_kabkota.*')
            ->join('tabel_prov', 'tabel_prov.id_prov = tabel_kabkota.id_prov')
            ->findAll();
        $this->data['kabkota']     = $kabkota;


        $model_referensi_spd = new model_referensi_spd();
        $referensi_st = $model_referensi_spd->find(1);
        $this->data['referensi_st']     = $referensi_st;


        $model_mak = new model_mak();
        $mak = $model_mak
            ->where('aktif', 1)
            ->asObject()
            ->findAll();
        $this->data['mak']     = $mak;

        $this->data['page']     = 'Surat Perintah Dinas';
        echo view('perjadin/v-spd', $this->data);
    }

    public function simpanSPD()
    {
        $model_spd = new model_spd();
        $validate = $this->validate([
            'tgl_spd' => 'required',
            'id_mak' => 'required',
        ]);
        if ($validate) {
            $tgl_spd = date_format(date_create($this->request->getPost('tgl_spd')), "Y-m-d");
            $tgl_kuitansi = date_format(date_create($this->request->getPost('tgl_kuitansi')), "Y-m-d");

            $datasimpan = $this->request->getPost();
            if ($datasimpan['id_spd'] == 0) { //TAMBAH
                unset($datasimpan['id_spd']);
            }
            $datasimpan['tgl_spd'] = $tgl_spd;
            $datasimpan['tgl_kuitansi'] = $tgl_kuitansi;
            $datasimpan['creator'] = $this->data['userdata']->id;
            // print_r($datasimpan);

            if ($model_spd->protect(false)->save($datasimpan)) {
                save_log(NULL, 'spd_simpan', $model_spd->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
        } else {
            json_message('error', $this->validator);
        }
    }

    public function simpanBiaya()
    {
        $datasimpan = $this->request->getPost();
        $model_biaya = new model_biaya();
        if ($datasimpan['id_biaya'] == 0) { //TAMBAH
            $validate = $this->validate([
                'kode_biaya' => 'required',
                'rincian_biaya' => 'required',
                'nominal_biaya' => 'required',
                'biaya_id_tertugas' => 'required',
                'biaya_id_st' => 'required',
                'id_biaya' => 'required',
            ]);
        } else { //EDIT
            $validate = $this->validate([
                'rincian_biaya' => 'required',
                'nominal_biaya' => 'required',
                'urutan' => 'required',
                'id_biaya' => 'required',
            ]);
        }
        if ($validate) {
            $tgl_spd = date_format(date_create($this->request->getPost('tgl_spd')), "Y-m-d");

            if ($datasimpan['id_biaya'] == 0) { //TAMBAH
                unset($datasimpan['id_biaya']);
                $datasimpan['creator'] = $this->data['userdata']->id;
                $datasimpan['id_tertugas'] =  $datasimpan['biaya_id_tertugas'];
                $datasimpan['id_st'] = $datasimpan['biaya_id_st'];
                unset($datasimpan['biaya_id_st']);
                unset($datasimpan['biaya_id_tertugas']);
            }
            $datasimpan['nominal_biaya'] = preg_replace("/[^0-9]/", "", $datasimpan['nominal_biaya']);



            // print_r($datasimpan);

            if ($model_biaya->protect(false)->save($datasimpan)) {
                save_log(NULL, 'biaya_simpan', $model_biaya->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
        } else {
            json_message('error', $this->validator);
        }
    }

    public function hapusBiaya($id)
    {
        $model_biaya = new model_biaya();
        if ($id == $this->request->getPost('id')) {
            if ($model_biaya->delete($this->request->getPost('id'), false)) {
                save_log(NULL, 'biaya_hapus', $model_biaya->getLastQuery()->getQuery());
                json_message('success', 'Data berhasil dihapus');
            } else {
                json_message('error', 'Data gagal dihapus');
            }
        }
    }
}
