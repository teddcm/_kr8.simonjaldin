<?php

namespace App\Controllers\referensi;

use App\Controllers\BaseController;
use App\Models\model_mak;

class Mak extends BaseController
{
    public function __construct()
    {
        $this->ionAuth    = new \IonAuth\Libraries\IonAuth();
        $this->validation = \Config\Services::validation();
        $this->data['userdata'] = $this->ionAuth->user()->row();
        helper(['form', 'url', 'main', 'log']);
    }

    public function index()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to(base_url('auth/signin'));
        }
        $this->data['page']     = 'Referensi Mata Anggaran Keluaran';
        echo view('index/0-top', $this->data);
        echo view('referensi/vr-mak', $this->data);
        echo view('index/0-bottom');
    }

    public function simpan()
    {
        $model_mak = new model_mak();
        $validate = $this->validate([
            'id_mak' => 'required',
            'detail_mak' => 'required',
            'tahun_mak' => 'required',
        ]);
        if ($validate) {
            $datasimpan = $this->request->getPost();
            if ($datasimpan['id_mak'] == 0) { //TAMBAH
                unset($datasimpan['id_mak']);
            }
            // print_r($datasimpan);

            if ($model_mak->protect(false)->save($datasimpan)) {
                json_message('success', 'Data berhasil disimpan');
            } else {
                json_message('error', 'Data gagal disimpan');
            }
        } else {
            json_message('error', $this->validator);
        }
    }
}
