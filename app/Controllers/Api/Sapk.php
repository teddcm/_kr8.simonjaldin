<?php

namespace App\Controllers\api;

use App\Controllers\BaseController;

class Sapk extends BaseController
{
    public function __construct()
    {

        /*

NO	SERVICE_ID	PATH
SERVICE_ID	PATH
ID-PNS-DATAUTAMA	            /api/pns/data-utama/{nipBaru} 
ID-PNS-RWGOLONGAN	            /api/pns/rw-golongan/{nipBaru}
ID-PNS-RWJABATAN	            /api/pns/rw-jabatan/{nipBaru}
ID-PNS-RWPMK	                /api/pns/rw-masakerja/{nipBaru}
ID-PNS-RWDIKLAT	                /api/pns/rw-diklat/{nipBaru}
ID-PNS-RWPENDIDIKAN	            /api/pns/rw-pendidikan/{nipBaru}
ID-PNS-RWPNSUNOR	            /api/pns/rw-pnsunor/{nipBaru}

ID-PNS-RWANGKAKREDIT	        /api/pns/rw-angkakredit/{nipBaru}
ID-PNS-RWSKP	                /api/pns/rw-skp/{nipBaru}

ID-PNS-RWCLTN	                /api/pns/rw-cltn/{nipBaru}
ID-PNS-RWPENGHARGAAN	        /api/pns/rw-penghargaan/{nipBaru}
ID-PNS-RWHUKDIS	                /api/pns/rw-hukdis/{nipBaru}
ID-PNS-RWDP3	                /api/pns/rw-dp3/{nipBaru}
ID-PNS-RWPWK	                /api/pns/rw-pwk/{nipBaru}
ID-PNS-RWPINDAHINSTANSI	        /api/pns/rw-pindahinstansi/{nipBaru}
ID-PNS-RWPEMBERHENTIAN	        /api/pns/rw-pemberhentian/{nipBaru}
ID-PNS-DATA-ORTU	            /api/pns/data-ortu/{nipBaru}

ID-PNS-DATA-PASANGAN	        /api/pns/data-pasangan/{nipBaru}
ID-PNS-DATA-ANAK	            /api/pns/data-anak/{nipBaru}


ID-PNS-KURSUS	                /api/pns/rw-kursus/{nipBaru}


ID-KPO-SK	                    /api/kpo/sk
ID-KPO-SK-HIST	                /api/kpo/sk/hist/{startDate}/{endDate}
ID-PPO-SK	                    /api/ppo/sk
ID-PPO-SK-HIST	                /api/ppo/sk/hist/{startDate}/{endDate}
ID-PPO-USULWAFAT	            /api/ppo/usul/wafat
ID-PPO-USULWAFAT-HIST	        /api/ppo/usul/wafat/hist/{startDate}/{endDate}
ID-PERE-UPDATED	                /api/updated/pns
ID-PERE-UPDATED-HIST	        /api/updated/hist/{startDate}/{endDate}
*/
    }

    function tokenizer($username = 'kanreg8client', $password = 'k4nr3g8')
    {
        $urlToken = 'https://wsrv-auth.bkn.go.id/oauth/token';
        $auth = base64_encode($username . ":" . $password);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlToken . "?client_id=" . $username . "&grant_type=client_credentials",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Origin: http://localhost:20000",
                "Authorization: Basic " . $auth
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $this->response->setContentType('Content-Type: application/json');
        echo $response;
        $json = json_decode($response, true);
        $token = $json['access_token'];
        $expires = $json['expires_in'];
        session_start();
        $_SESSION['tokenizer'] = $token;
    }

    function wsrv($nip = '199003192015031003', $servis = 'data-utama', $token = null)
    {
        session_start();
        $sestoken = $_SESSION['tokenizer'];
        $token = $token == null ? ($sestoken) : $token;
        $path = '/api/pns/' . $servis . '/' . $nip;
        $urlreq = 'https://wsrv.bkn.go.id';
        if (!empty($token)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $urlreq  . $path,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "Origin: http://localhost:20000",
                    "Authorization: Bearer " . $token
                ),
            ));
            $result = curl_exec($curl);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }
            curl_close($curl);
            if (isset($error_msg)) {
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            } else {
                $this->response->setContentType('Content-Type: application/json');
                $data = (json_decode($result));
                if ($servis == 'data-utama') {
                    echo ($data->data);
                } else {
                    echo json_encode($data->data);
                }
            }
        }
    }
}
